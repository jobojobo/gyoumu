/**
 * @fileoverview 確認画面表示用submit
 * @author 自分の名前
 * @version 1.0
 * @date    H2X. NN. NN
 */

function ConfarmPage(formName,url) {
    // サブミットするフォームを取得
    var form = document.forms[formName];
    form.method = 'POST'; // method(GET or POST)を設定する
    form.action = url;    // action(遷移先URL)を設定する
    form.target = "conf_page"; // 表示先のウィンドウを設定する
    form.submit();        // submit する
    return true;

}