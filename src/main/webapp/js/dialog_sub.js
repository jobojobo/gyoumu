/**
 * @fileoverview 確認ダイアログ
 * @author 自分の名前
 * @version 1.0
 * @date    H2X. NN. NN
 */

$( function() {
     // dialog_buttonクラスのボタンが押されたときにダイアログオープン
     $( '.dialog_button' ) . click( function() {
         jQuery( '#dialog' ) . dialog( 'open' );
     } );
     // id=dialog要素のダイアログパラメータ設定
     $( '#dialog' ) . dialog( {
         autoOpen: false, // ダイアログを自動表示しない
         show: 'explode', // ダイアログが表示される際のエフェクト
         hide: 'explode', // ダイアログを閉じる際のエフェクト
         modal: true,     // モーダル・ダイアログ
         buttons: {       // ダイアログにボタンを追加
             '送信': function() {
                 $( this ) . dialog( 'close' );
                 $( '#delform' ).submit();
             },
             'キャンセル': function() {
                 jQuery( this ) . dialog( 'close' );
             },
         }
     } );
} );
