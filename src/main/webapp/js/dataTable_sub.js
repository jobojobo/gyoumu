/**
 * @fileoverview dataTableパラメータ設定js
 * @author 自分の名前
 * @version 1.0
 * @date    H2X. NN. NN
 */

$(function(){
	  $('#syain_table').dataTable( {
	    "bFilter": false,
	    "aLengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]]
	  });
});