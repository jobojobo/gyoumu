﻿/**
 * @fileoverview 入力データチェックスクリプト
 * @author 自分の名前
 * @version 1.0
 * @date    H2X. NN. NN
 */

/* jQueryのreadyイベント */
$(function () {
  var msgs=[];  // エラーメッセージを格納する配列
  
  /* エラーメッセージ格納関数 */
  function setError (elem, msg) {
    msgs.push('<li>' + msg + '</li>'); // 配列にエラーメッセージを追加
    $(elem).addClass('error_field');   // エレメントに'error_field'クラスを追加
  };
  
  /* formのsubmitイベント */
  $('form').submit(function(e){
      msgs = []; // エラーメッセージを格納する配列
      /* validクラスに対する操作 */
      $('.valid', this)
      .removeClass('error_field') // エレメントの'error_field'クラスを削除
       /* requiredクラスに対する操作(メソッドチェーン) */
      .filter('.required')
      .each(function () { // requiredに合致した全てのエレメントに対して以下を実行
        if ($(this).val() === '') {
          setError(this, $(this).prev('label').text() + 'は必須入力です。'); // setError関数の呼び出し
        }
      })
      .end() // メソッドチェーンを1つ破棄して1つ前の状態に戻す
      /* 3figureクラスに対する操作(メソッドチェーン) */
      .filter('.3figure')
      .each(function () { // 3figureに合致した全てのエレメントに対して以下を実行
        if ($(this).val().length != 3 ) {
          setError(this, $(this).prev('label').text() + 'は３桁で入力してください。');
        }
      })
      .end() // メソッドチェーンを1つ破棄して1つ前の状態に戻す
      /* 5figureクラスに対する操作(メソッドチェーン) */
      .filter('.5figure')
      .each(function () { // 5figureに合致した全てのエレメントに対して以下を実行
        if ($(this).val().length != 5 ) {
          setError(this, $(this).prev('label').text() + 'は５桁で入力してください。'); // setError関数の呼び出し
        }
      })
      .end() // メソッドチェーンを1つ破棄して1つ前の状態に戻す
      /* numberクラスに対する操作(メソッドチェーン) */
      .filter('.number')
      .each(function () { // numberに合致した全てのエレメントに対して以下を実行
        if (!$.isNumeric($(this).val())) {
          setError(this,
            $(this).prev('label').text() + 'は数値で入力してください。');
        }
      });
      /* エラーメッセージ無しの場合 */
      if (msgs.length === 0) {
        $('#error_summary').css('display', 'none');
        return true;
      /* エラーメッセージありの場合 */
      } else {
        $('#error_summary')
          .css('display', 'block')
          .html(msgs.join('')); // 配列を連結しhtmlとして出力する
        e.preventDefault();  // formのデータ送信を無効
      }
  });
});