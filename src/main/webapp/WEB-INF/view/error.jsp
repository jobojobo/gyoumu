﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="org.apache.struts.Globals"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<s:link styleClass="general-button" href="/">戻る[F8]</s:link>
				<h3>エラー画面</h3>
				<html:errors />
				<hr />
				<logic:present name="<%=Globals.EXCEPTION_KEY%>">
					<bean:write name="<%=Globals.EXCEPTION_KEY%>" />
				</logic:present>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>

<global-exceptions>
		<exception type="org.seasar.framework.exception.SQLRuntimeException" key="errors.fk"
			path="/WEB-INF/view/error.jsp" />
		<exception type="java.lang.NullPointerException" key="errors.null"
			path="/WEB-INF/view/error.jsp" />
		<exception type="java.lang.NumberFormatException" key="errors.numberformat"
			path="/WEB-INF/view/error.jsp" />
	</global-exceptions>