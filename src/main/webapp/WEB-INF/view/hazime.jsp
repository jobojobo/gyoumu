<%--                                        --%>
<%--  index.jsp  トップページ           		        --%>
<%--                                        --%>
<%--  @author  bigbridgeLTD                 --%>
<%--  @date    2017/06/16                   --%>
<%--                                        --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<script>
	document.onkeydown = pageMove;

	function pageMove() {
		if (event.keyCode == 49) //作品管理「1」が押されたか確認
		{
			window.location.href = '/Gyoumu/menu';
		}
		if (event.keyCode == 50) //従業員管理「2」が押されたか確認
		{
			window.location.href = '/Gyoumu/hurikaeme';
		}
		if (event.keyCode == 119) //「F8」が押されたか確認
		{
			history.back();
		}
	}
</script>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<table class="basetale">
					<tr>
						<th><h2 class="menu">会員管理[1]</h2></th>
						<th><h2 class="menu">振替管理[2]</h2></th>
					</tr>
				</table>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>