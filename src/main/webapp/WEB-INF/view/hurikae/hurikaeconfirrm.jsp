<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
		flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<h3>振替情報確認</h3>
				<div class="center">
					<h2>振替内容を確認してください</h2>
					<s:form method="POST">
						<table class="basetable">
							<tr>
								<th>DVD番号</th>
								<td>${f:h(dvdno)}</td>
								<html:hidden property="dvdno" value="${f:h(dvdno)}" />
							</tr>
							<tr>
								<th class="center">店舗番号</th>
								<td>${f:h(tenpono)}</td>
								<html:hidden property="tenpono"
									value="${f:h(tenpono)}" />
							</tr>
						</table>

						<div class="center">
							<s:submit property="fin" styleClass="button" value="店舗間移動" />

						</div>
					</s:form>
				</div>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>