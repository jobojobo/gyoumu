<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>
    <tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
        flush="true">
        <tiles:put name="title" value="HCSビデオレンタルシステム" />
        <tiles:put name="content" type="string">
            <article>
                <h3>振替登録</h3>
                <div class="center">
                    <h2>振替登録が完了しました。</h2>
                    <hr />
                    <p>
                        <br />システムにログインできない場合はシステム管理者までお問い合わせください。
                    </p>
                    <address>kanri@xxx.co.jp</address>
                </div>
            </article>
        </tiles:put>
    </tiles:insert>
</body>
</html>