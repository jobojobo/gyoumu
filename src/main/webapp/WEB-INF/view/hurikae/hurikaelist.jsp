<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>

	<tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
		flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<s:form method="POST">
					<h3>振替一覧</h3>
					<table border="1">
						<thead>
							<tr>
								<th>DVD番号</th>
								<th>作品名</th>
								<th>貸出ステータス</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="hurikae" varStatus="c" items="${hurikaeList}">
								<tr>
									<td>${f:h(hurikae.dvdno)}</td>
									<td>${f:h(hurikae.title)}</td>
									<td>${f:h(hurikae.kasidasistatus)}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>