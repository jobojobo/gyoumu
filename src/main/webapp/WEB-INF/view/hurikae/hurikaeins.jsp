<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
		flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<h3>振替画面</h3>
				<html:errors />
				<s:form method="POST">
					<table class="basetable">
						<tr>
							<th>DVD番号</th>
							<td><html:text property="dvdno" value="" /></td>
							<html:hidden property="dvdno"
								value="${f:h(dvdno)}" />
						</tr>
						<tr>
							<th>店舗番号</th>
							<td><html:text property="tenpono" value="" /></td>
						</tr>
					</table>
					<div class="center">
						<s:submit property="huco" styleClass="general-button" value="確認" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>