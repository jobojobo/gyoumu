<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/kensakulauout2.jsp"
		flush="true">
		<tiles:put name="title" value="レジシステム" />
		<tiles:put name="content" type="string">
			<html:errors />
			
			<form method="POST" Id="loginform">
				<article>
					<p>店員番号を入力しログインを行ってください</p>

					<table class="basetable">
						<tr>
							<th>レジ番号 <html:img src="${f:url('/images/required.jpg')}" /></th>
							<td><html:text property="rejino" value="" /></td>
						</tr>
						<tr>
							<th>店員番号 <html:img src="${f:url('/images/required.jpg')}"
									alt="必須" /></th>
							<td><html:text property="teninno" value="" /></td>
						</tr>
					</table>
					<div class="center">
						<s:submit property="loginconfirm" styleClass="button" value="ログイン" />
						<html:button styleClass="button" property=""
							onclick="this.form.reset();" value="内容をクリア" />
					</div>
				</article>
			</form>
		</tiles:put>
	</tiles:insert>
</body>
</html>