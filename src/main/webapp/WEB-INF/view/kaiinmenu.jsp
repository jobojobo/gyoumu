<%--                                        --%>
<%--  index.jsp  トップページ                         --%>
<%--                                        --%>
<%--  @author  bigbridgeLTD                 --%>
<%--  @date    2017/06/16                   --%>
<%--                                        --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<script>
	document.onkeydown = pageMove;

	function pageMove() {
		if (event.keyCode == 49) //作品管理「1」が押されたか確認
		{
			window.location.href = '/Gyoumu/kai';
		}
		if (event.keyCode == 50) //従業員管理「2」が押されたか確認
		{
			window.location.href = '/Gyoumu/tou';
		}
		if (event.keyCode == 51) //DVD管理「3」が押されたか確認
		{
			window.location.href = '/Gyoumu/rire';
		}
		if (event.keyCode == 52) //レジ管理「4」が押されたか確認
		{
			window.location.href = '/Gyoumu/entry';
		}
		if (event.keyCode == 53) //店舗管理「5」が押されたか確認
		{
			window.location.href = '/Gyoumu/elist';
		}
		if (event.keyCode == 54) //従業員管理「6」が押されたか確認
		{
			window.location.href = '/Gyoumu/kariken';
		}
		if (event.keyCode == 55) //ブラック会員一覧表示「7」が押されたか確認
		{
			window.location.href = '/Gyoumu/black';
		}
		if (event.keyCode == 56) //取り置き一覧「8」が押されたか確認
		{
			window.location.href = '/Gyoumu/toriokilist';
		}
		if (event.keyCode == 119) //「F8」が押されたか確認
		{
			history.back();
		}
	}
</script>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<table class="basetale">
					<tr>
						<th><h2 class="menu">会員検索、変更[1]</h2></th>
						<th><h2 class="menu">会員登録[2]</h2></th>
						<th><h2 class="menu">貸出履歴[3]</h2></th>
						<th><h2 class="menu">未納延滞金更新[4]</h2></th>
						<th><h2 class="menu">延滞表示[5]</h2></th>
						<th><h2 class="menu">仮会員検索[6]</h2></th>
						<th><h2 class="menu">ブラック会員一覧[7]</h2></th>
						<th><h2 class="menu">取り置き一覧[8]</h2></th>
					</tr>
				</table>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>