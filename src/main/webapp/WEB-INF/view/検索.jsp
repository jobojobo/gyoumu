<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
		flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<h3>作品検索画面</h3>
				<html:errors />
				<s:form method="POST">
					<table class="basetable">
						<tr>
							<th>タイトル</th>
							<td><html:text property="title" value="" /></td>
						</tr>
						<tr>
							<th>ジャンル名</th>
							<td><select name="jyanruno">
									<option></option>
									<option value='1     '>アクション</option>
									<option value='2     '>アダルト</option>
									<option value='3     '>アニメ</option>
									<option value='4     '>SF</option>
									<option value='5     '>お笑い</option>
									<option value='6     '>音楽</option>
									<option value='7     '>キッズ</option>
									<option value='8     '>コメディ</option>
									<option value='9     '>スポーツ</option>
									<option value='10    '>ドラマ</option>
									<option value='11    '>ホラー</option>
							</select></td>
						</tr>
						<tr>
							<th>新作区分</th>
							<td><select name="sinsakukubun">
									<option></option>
									<option value="1">新作</option>
									<option value="2">準新作</option>
									<option value="3">旧作</option>
							</select></td>
						</tr>
						<tr>
							<th>主 演</th>
							<td><html:text property="shuen" value="" /></td>
						</tr>
						<tr>
							<th>監 督</th>
							<td><html:text property="kantoku" value="" /></td>
						</tr>
					</table>
					<div class="center">
						<s:submit property="list" styleClass="general-button" value="検索" />
						<html:button styleClass="general-button" property=""
							onclick="this.form.reset();" value="内容をクリア" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>