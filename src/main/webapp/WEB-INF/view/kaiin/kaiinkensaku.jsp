<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>

	<tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
		flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<s:form method="POST">
					<h3>会員検索結果画面</h3>
					<table border="1">
						<thead>
							<tr>
								<th>会員番号</th>
								<th>会員名</th>
								<th>会員 かな</th>
								<th>性別</th>
								<th>住所</th>
								<th>電話番号</th>
								<th>メールアドレス</th>
								<th>誕生日</th>
								<th>職業</th>
								<th>身分証種別</th>
								<th>身分証番号</th>
								<th>退会フラグ</th>
								<th>発行日</th>
								<th>ブラック会員フラグ</th>
								<th>延滞カウント</th>
								<th>未納延滞金</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="kaiin" varStatus="c" items="${kaiinList}">
								<tr>
									<html:hidden property="kaiinno" value="${f:h(kaiin.kaiinno)}" />
									<td>${f:h(kaiin.kaiinno)}</td>
									<html:hidden property="kaiinname"
										value="${f:h(kaiin.kaiinname)}" />
									<td>${f:h(kaiin.kaiinname)}</td>
									<html:hidden property="kaiinhurigana"
										value="${f:h(kaiin.kaiinhurigana)}" />
									<td>${f:h(kaiin.kaiinhurigana)}</td>
									<html:hidden property="gender" value="${f:h(kaiin.gender)}" />
									<td>${f:h(kaiin.gender)}</td>
									<html:hidden property="address" value="${f:h(kaiin.address)}" />
									<td>${f:h(kaiin.address)}</td>
									<html:hidden property="tel" value="${f:h(kaiin.tel)}" />
									<td>${f:h(kaiin.tel)}</td>
									<html:hidden property="mail" value="${f:h(kaiin.mail)}" />
									<td>${f:h(kaiin.mail)}</td>
									<html:hidden property="birthday" value="${f:h(kaiin.birthday)}" />
									<td>${f:h(kaiin.birthday)}</td>
									<html:hidden property="shokugyo" value="${f:h(kaiin.shokugyo)}" />
									<td>${f:h(kaiin.shokugyo)}</td>
									<html:hidden property="mibunshokubun"
										value="${f:h(kaiin.mibunshokubun)}" />
									<td>${f:h(kaiin.mibunshokubun)}</td>
									<html:hidden property="mibunshono"
										value="${f:h(kaiin.mibunshono)}" />
									<td>${f:h(kaiin.mibunshono)}</td>
									<html:hidden property="taikaiflg"
										value="${f:h(kaiin.taikaiflg)}" />
									<td>${f:h(kaiin.taikaiflg)}</td>
									<html:hidden property="hakkouday"
										value="${f:h(kaiin.hakkouday)}" />
									<td>${f:h(kaiin.hakkouday)}</td>
									<html:hidden property="blackflg" value="${f:h(kaiin.blackflg)}" />
									<td>${f:h(kaiin.blackflg)}</td>
									<html:hidden property="entai"
										value="${f:h(kaiin.threeentaicount)}" />
									<td>${f:h(kaiin.threeentaicount)}</td>
									<html:hidden property="minou"
										value="${f:h(kaiin.minouentaiken)}" />
									<td>${f:h(kaiin.minouentaiken)}</td>
									<td><s:submit property="kensaku2"
											styleClass="general-button" value="変更" /></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>