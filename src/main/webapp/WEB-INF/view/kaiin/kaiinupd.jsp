<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
		flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<h3>作品検索画面</h3>
				<html:errors />
				<s:form method="POST">
					<table class="basetable">
						<c:forEach var="kaiin" varStatus="c" items="${kaiinList}">
							<tr>
								<th>会員番号</th>
								<html:hidden property="kaiinno" value="${f:h(kaiin.kaiinno)}" />
								<td>${f:h(kaiin.kaiinno)}</td>
							</tr>
							<tr>
								<th>会員名</th>
								<td><html:text property="kaiinname"
										value="${f:h(kaiin.kaiinname)}" /></td>
							</tr>
							<tr>
								<th>会員名 かな</th>
								<td><html:text property="kaiinhurigana"
										value="${f:h(kaiin.kaiinhurigana)}" /></td>
							</tr>
							<tr>
								<th>性別</th>
								<td>${f:h(kaiin.gender)}</td>
							</tr>
							<tr>
								<th>住所</th>
								<td><html:text property="address"
										value="${f:h(kaiin.address)}" /></td>
							</tr>
							<tr>
								<th>電話番号</th>
								<td><html:text property="tel" value="${f:h(kaiin.tel)}" /></td>
							</tr>
							<tr>
								<th>メールアドレス</th>
								<td><html:text property="mail" value="${f:h(kaiin.mail)}" /></td>
							</tr>
							<tr>
								<th>誕生日</th>
								<td>${f:h(kaiin.birthday)}</td>
							</tr>
							<tr>
								<th>職業</th>
								<td><html:text property="shokugyo"
										value="${f:h(kaiin.shokugyo)}" /></td>
							</tr>
							<tr>
								<th>身分証区分</th>
								<td><c:choose>
										<c:when test="${kaiin.mibunshokubun==01}">運転免許証</c:when>
										<c:when test="${kaiin.mibunshokubun==02}">健康保険証</c:when>
										<c:when test="${kaiin.mibunshokubun==03}">学生証</c:when>
										<c:when test="${kaiin.mibunshokubun==04}">パスポート</c:when>
									</c:choose></td>
							</tr>
							<tr>
								<th>身分証番号</th>
								<td>${f:h(kaiin.mibunshono)}</td>
							</tr>
							<tr>
								<th>退会フラグ</th>
								<td><select name="taikaiflg">
										<option value='0' selected>会員</option>
										<option value='1'>退会済</option>
								</select></td>
							</tr>
							<tr>
								<th>発行日</th>
								<td>${kaiin.hakkouday}</td>
							</tr>
							<tr>
								<th>ブラック会員フラグ</th>
								<td><select name="blackflg">
										<option value='0' selected>正常</option>
										<option value='1'>ブラック会員</option>
								</select></td>
							</tr>
							<tr>
								<th>延滞カウント</th>
								<td>${f:h(kaiin.threeentaicount)}</td>
							</tr>
							<tr>
								<th>未納延滞金</th>
								<td>${f:h(kaiin.minouentaiken)}</td>
							</tr>
						</c:forEach>
					</table>
					<div class="center">
						<s:submit property="upd" styleClass="general-button" value="更新" />
						<html:button styleClass="general-button" property=""
							onclick="this.form.reset();" value="内容をクリア" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>