<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>

    <tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
        flush="true">
        <tiles:put name="title" value="HCSビデオレンタルシステム" />
        <tiles:put name="content" type="string">
            <article>
                <s:form method="POST">
                    <h3>取り置き一覧</h3>
                    <table border="1">
                        <thead>
                            <tr>
                                <th>DVD番号</th>
                                <th>作品名</th>
                                <th>媒体</th>
                                <th>店舗番号</th>
                                <th>電話番号</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="torioki" varStatus="c" items="${toriokiList}">
                                <tr>
                                    <td>${f:h(torioki.dvdno)}</td>
                                    <td>${f:h(torioki.title)}</td>
                                    <td>${f:h(torioki.baitai)}</td>
                                    <td>${f:h(torioki.tenpono)}</td>
                                    <td>${f:h(torioki.tel)}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </s:form>
            </article>
        </tiles:put>
    </tiles:insert>
</body>
</html>