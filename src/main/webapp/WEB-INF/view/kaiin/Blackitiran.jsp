<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>

    <tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
        flush="true">
        <tiles:put name="title" value="HCSビデオレンタルシステム" />
        <tiles:put name="content" type="string">
            <article>
                <s:form method="POST">
                    <h3>ブラック会員検索結果画面</h3>
                    <table border="1">
                        <thead>
                            <tr>
                                <th>会員番号</th>
                                <th>会員名</th>
                                <th>会員ふりがな</th>
                                <th>性別</th>
                                <th>住所</th>
                                <th>電話番号</th>
                                <th>メールアドレス</th>
                                <th>生年月日</th>
                                <th>職業</th>
                                <th>身分証区分</th>
                                <th>身分証番号</th>
                                <th>退会フラグ</th>
                                <th>発行日</th>
                                <th>ブラックフラグ</th>
                                <th>延滞カウント</th>
                                <th>未納延滞金</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="rireki" varStatus="c" items="${blackList}">
                                <tr>
                                    <td>${f:h(rireki.kaiinno)}</td>
                                    <td>${f:h(rireki.kaiinname)}</td>
                                    <td>${f:h(rireki.kaiinhurigana)}</td>
                                    <td>${f:h(rireki.gender)}</td>
                                    <td>${f:h(rireki.address)}</td>
                                    <td>${f:h(rireki.tel)}</td>
                                    <td>${f:h(rireki.mail)}</td>
                                    <td>${f:h(rireki.birthday)}</td>
                                    <td>${f:h(rireki.shokugyo)}</td>
                                    <td>${f:h(rireki.mibunshokubun)}</td>
                                    <td>${f:h(rireki.mibunshono)}</td>
                                    <td>${f:h(rireki.taikaiflg)}</td>
                                    <td>${f:h(rireki.hakkouday)}</td>
                                    <td>${f:h(rireki.blackflg)}</td>
                                    <td>${f:h(rireki.threeentaicount)}</td>
                                    <td>${f:h(rireki.minouentaiken)}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </s:form>
            </article>
        </tiles:put>
    </tiles:insert>
</body>
</html>