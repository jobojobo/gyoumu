<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>
    <tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
        flush="true">
        <tiles:put name="title" value="HCSビデオレンタルシステム" />
        <tiles:put name="content" type="string">
            <article>
                <h3>会員検索画面</h3>
                <html:errors />
                <s:form method="POST">
                    <table class="basetable">
                        <tr>
                            <th>電話番号</th>
                            <td><html:text property="tel" value="" /></td>
                            <html:hidden property="tel" value="tel" />
                        </tr>
                    </table>
                    <div class="center">
                        <s:submit property="karikaiinins" styleClass="general-button" value="検索" />
                        <html:button styleClass="general-button" property=""
                            onclick="this.form.reset();" value="内容をクリア" />
                    </div>
                </s:form>
            </article>
        </tiles:put>
    </tiles:insert>
</body>
</html>