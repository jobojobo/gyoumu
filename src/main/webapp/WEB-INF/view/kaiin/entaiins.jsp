<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
		flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<h3>未納延滞金更新画面</h3>
				<html:errors />
				<s:form method="POST">
					<table class="basetable">
						<tr>
							<th>会員番号</th>
							<td><html:text property="kaiinno" value="" /></td>
						</tr>
						<tr>
							<th>未納延滞金</th>
							<td><html:text property="minouentaiken" value="" /></td>
						</tr>
					</table>
					<div class="center">
						<s:submit property="ins2" styleClass="general-button"
							value="変更" />
						<html:button styleClass="general-button" property=""
							onclick="this.form.reset();" value="内容をクリア" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>