<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
		flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<h3>会員登録画面</h3>
				<html:errors />
				<s:form method="POST">
					<table class="basetable">
						<tr>
							<th>会員名</th>
							<td><html:text property="kaiinname" value="" /></td>
						</tr>
						<tr>
							<th>会員名 かな</th>
							<td><html:text property="kaiinhurigana" value="" /></td>
						</tr>
						<tr>
							<th>性別</th>
							<td><select name="gender">
									<option value='男' selected>男</option>
									<option value='女'>女</option>
							</select></td>
						</tr>
						<tr>
							<th>住所</th>
							<td><html:text property="address" value="" /></td>
						</tr>
						<tr>
							<th>電話番号</th>
							<td><html:text property="tel" value="" /></td>
						</tr>
						<tr>
							<th>メールアドレス</th>
							<td><html:text property="mail" value="" /></td>
						</tr>
						<tr>
							<th>誕生日</th>
							<td><html:text property="birthday" value="" /></td>
						</tr>
						<tr>
							<th>職業</th>
							<td><html:text property="shokugyo" value="" /></td>
						</tr>
						<tr>
							<th>身分証区分</th>
							<td><select name="mibunshokubun">
									<option value='01' selected>運転免許証</option>
									<option value='02'>健康保険証</option>
									<option value='03'>学生証</option>
									<option value='04'>パスポート</option>
							</select></td>
						</tr>
						<tr>
							<th>身分証番号</th>
							<td><html:text property="mibunshono" value="" /></td>
						</tr>
					</table>
					<div class="center">
						<s:submit property="ins" styleClass="general-button" value="登録" />
						<html:button styleClass="general-button" property=""
							onclick="this.form.reset();" value="内容をクリア" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>