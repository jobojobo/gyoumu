<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
    <tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
        flush="true">
        <tiles:put name="title" value="社員情報管理システム" />
        <tiles:put name="content" type="string">
            <article>
            <h3>会員情報登録</h3>
            <table border="1">
                        <thead>
                            <tr>
                                <th>会員番号</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="kaiin" varStatus="c" items="${kaiinList}">
                                <tr>
                                    <td>${f:h(kaiin.kaiinno)}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
            <div class="center">
                <h2>更新が完了しました。</h2>
                <hr />
                <p>
                    <br />システムにログインできない場合はシステム管理者までお問い合わせください。
                </p>
                <address>kanri@xxx.co.jp</address>
            </div>
            </article>
        </tiles:put>
    </tiles:insert>
</body>
</html>