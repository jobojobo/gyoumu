<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>

	<tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
		flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<s:form method="POST">
					<h3>未納延滞者一覧</h3>
					<table border="1">
						<thead>
							<tr>
								<th>会員番号</th>
								<th>会員名</th>
								<th>電話番号</th>
								<th>未納延滞金</th>
								<th>作品名</th>
								<th>返却予定日</th>
								<th>店舗名</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="entai" varStatus="c" items="${entaiList}">
								<tr>
									<td>${f:h(entai.kaiinno)}</td>
									<td>${f:h(entai.kaiinname)}</td>
									<td>${f:h(entai.tel)}</td>
									<td>$(entai.minouentaiken)</td>
									<td>${f:h(entai.title)}</td>
									<td>${f:h(entai.henkyakuyoteiday)}</td>
									<td>${f:h(entai.tenponame)}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>