<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>

	<tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
		flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<s:form method="POST">
					<h3>会員検索結果画面</h3>
					<table border="1">
						<thead>
							<tr>
								<th>会計番号</th>
								<th>貸出明細番号</th>
								<th>DVD番号</th>
								<th>返却日</th>
								<th>返却予定日</th>
								<th>拍数</th>
								<th>単価</th>
								<th>割引番号</th>
								<th>会計区分</th>
								<th>会計年月日</th>
								<th>金額
								<th>レジ番号</th>
								<th>店員番号</th>
								<th>店舗番号</th>
								<th>会員番号</th>
								<th>返金元会計番号</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="rireki" varStatus="c" items="${rirekiList}">
								<tr>
									<td>${f:h(rireki.kaikeino)}</td>
									<td>${f:h(rireki.kasidasimeisaino)}</td>
									<td>${f:h(rireki.dvdno)}</td>
									<td>${f:h(rireki.henkyakuday)}</td>
									<td>${f:h(rireki.henkyakuyoteiday)}</td>
									<td>${f:h(rireki.hakusu)}</td>
									<td>${f:h(rireki.tanka)}</td>
									<td>${f:h(rireki.waribikino)}</td>
									<td>${f:h(rireki.kaikeikubun)}</td>
									<td>${f:h(rireki.kaikeinengappi)}</td>
									<td>${f:h(rireki.kingaku)}</td>
									<td>${f:h(rireki.rejino)}</td>
									<td>${f:h(rireki.teninno)}</td>
									<td>${f:h(rireki.tenpono)}</td>
									<td>${f:h(rireki.kaiinno)}</td>
									<td>${f:h(rireki.henkinno)}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>