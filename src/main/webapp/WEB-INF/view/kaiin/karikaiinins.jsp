<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/kensakulayout.jsp"
		flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<h3>会員変更画面</h3>
				<html:errors />
				<s:form method="POST">
					<table class="basetable">
						<c:forEach var="kari" varStatus="c" items="${kariList}">
							<tr>
								<html:hidden property="karikaiinno"
									value="${f:h(kari.karikaiinno)}" />
								<th>会員名</th>
								<td><html:text property="kaiinname"
										value="${f:h(kari.kaiinname)}" /></td>
							</tr>
							<tr>
								<th>会員名 かな</th>
								<td><html:text property="kaiinhurigana"
										value="${f:h(kari.kaiinhurigana)}" /></td>
							</tr>
							<tr>
								<th>性別</th>
								<td><select name="gender">
										<option value='男' selected>男</option>
										<option value='女'>女</option>
								</select></td>
							</tr>
							<tr>
								<th>住所</th>
								<td><html:text property="address"
										value="${f:h(kari.address)}" /></td>
							</tr>
							<tr>
								<th>電話番号</th>
								<td><html:text property="tel" value="${f:h(kari.tel)}" /></td>
							</tr>
							<tr>
								<th>メールアドレス</th>
								<td><html:text property="mail" value="${f:h(kari.mail)}" /></td>
							</tr>
							<tr>
								<th>誕生日</th>
								<td><html:text property="birthday" value="" /></td>
							</tr>
							<tr>
								<th>職業</th>
								<td><html:text property="shokugyo"
										value="${f:h(kari.shokugyo)}" /></td>
							</tr>
							<tr>
								<th>身分証区分</th>
								<td><select name="mibunshokubun">
										<option value='01' selected>運転免許証</option>
										<option value='02'>健康保険証</option>
										<option value='03'>学生証</option>
										<option value='04'>パスポート</option>
								</select></td>
							</tr>
							<tr>
								<th>身分証番号</th>
								<td><html:text property="mibunshono"
										value="${f:h(kari.mibunshono)}" /></td>
							</tr>
						</c:forEach>
					</table>
					<div class="center">
						<s:submit property="ins3" styleClass="general-button" value="変更" />
						<html:button styleClass="general-button" property=""
							onclick="this.form.reset();" value="内容をクリア" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>