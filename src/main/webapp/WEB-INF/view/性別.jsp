<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/karilayout.jsp"
		flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<h3>仮会員情報入力フォーム</h3>
				<html:errors />
				<s:form method="POST">
					<table class="basetable">
						<tr>
							<th>氏 名</th>
							<td><html:text property="kaiinname" /></td>
							<th class="center">ふりがな</th>
							<td><html:text property="kaiinhurigana" /></td>
						</tr>
						<tr>
							<th class="center">性 別</th>
							<td><html:radio property="gender" value="男性"
									styleClass="radio" />男性 <html:radio property="gender"
									value="女性" styleClass="radio" />女性</td>
							<th class="center">住 所</th>
							<td><input type="text" name="address" value=""></td>
						</tr>
						<tr>
							<th class="center">電話番号</th>
							<td><html:text property="tel" /></td>
							<th class="center">E-Mail</th>
							<td><html:text property="mail" /></td>
						</tr>
						<tr>
							<th class="center">生年月日</th>
							<td><html:text property="birthday" /></td>
							<th class="center">職 業</th>
							<td><html:text property="shokugyo" /></td>
						</tr>
					</table>
					<div class="center">
						<s:submit property="confirm" styleClass="general-button"
							value="確認画面へ" />
						<html:button styleClass="general-button" property="1regist"
							value="内容をクリア" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>

