<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<title><tiles:getAsString name="title" /></title>
<link rel="stylesheet" href="${f:url('/css/ranking.css')}">
<script type="text/javascript" src="${f:url('/js/jquery-1.9.1.min.js')}"></script>
<script type="text/javascript" src="${f:url('/js/mouseless.js')}"></script>
</head>
<body>
    <tiles:insert attribute="content" />
    <tiles:insert page="/WEB-INF/view/common/footer.jsp" />
</body>
</html>