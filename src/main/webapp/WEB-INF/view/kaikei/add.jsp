<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/dvdlayout.jsp"
		flush="true">
		<tiles:put name="title" value="HCSビデオレンタルシステム" />
		<tiles:put name="content" type="string">
			<article>
				<s:link styleClass="general-button" href="desc">戻る[F8]</s:link>
				<h3>会計登録画面</h3>
				<html:errors />
				<s:form method="POST">
					<table class="basetable">
						<tr>
							<th>会計区分</th>
							<td><html:select property="kaikeikubun">
									<c:forEach var="k" items="${shiharaikubunitems}">
										<html:option value="${f:h(k.kaikeikubun)}">${f:h(k.kaikeikubunname)}</html:option>
									</c:forEach>
								</html:select></td>
							<th>レジ番号</th>
							<td><html:select property="rejino">
									<c:forEach var="r" items="${rejiitems}">
										<html:option value="${f:h(r.rejino)}"></html:option>
									</c:forEach>
								</html:select></td>
						</tr>
						<tr>
							<th>金額</th>
							<td><html:text property="kingaku">
								</html:text></td>
							<th>会員番号</th>
							<td><html:text property="kaiinno">
								</html:text></td>
						</tr>
						<tr>
							<th>返金番号</th>
							<td><html:text property="henkinno">
								</html:text></td>
						</tr>
					</table>
					<div class="center">
						<s:submit property="fin" styleClass="general-button"
							value="登録[Enter]" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>