/*
 * Copyright 2004-2008 the Seasar Foundation and the Others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package gyoumu.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.seasar.framework.aop.annotation.RemoveSession;
import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import gyoumu.dto.HurikaeDto;
import gyoumu.dto.KaiinDto;
import gyoumu.dto.LoginDto;
import gyoumu.entity.Kaiin;
import gyoumu.entity.Kaiin4;
import gyoumu.entity.Dvd;
import gyoumu.entity.Dvd2;
import gyoumu.entity.Dvd3;
import gyoumu.entity.Tenin;
import gyoumu.entity.Reji;
import gyoumu.form.KaiinForm;
import gyoumu.form.LoginForm;
import gyoumu.service.HurikaeService;
import gyoumu.service.KaiinService;
import gyoumu.service.TeninService;

public class IndexAction {

	@Resource
	protected HttpServletRequest request;

	@Resource
	protected HttpServletResponse response;

	@Resource
	protected HttpSession session;

	@Resource
	protected ServletContext application;

	public List<Kaiin> kaiinList;

	public List<Kaiin> rirekiList;
	public List<Dvd3> hurikaeList;
	public List<Kaiin4> entaiList;
	public List<Kaiin> blackList;
	public List<Dvd2> toriokiList;

	@Resource
	protected HurikaeDto hurikaeDto;

	@Resource
	protected LoginDto loginDto;

	@Resource
	@ActionForm
	protected LoginForm loginform;

	@Resource
	protected KaiinService kaiinService;

	@Resource
	protected TeninService teninService;

	@Resource
	protected HurikaeService hurikaeservice;

	@Resource
	@ActionForm
	protected KaiinForm kaiinform;

	@Resource
	protected KaiinDto kaiinDto;

	private String kaiinno = "";

	private String rejino = "";

	public String getRejino(String rejino) {
		return rejino;
	}

	public void setRejino(String rejino) {
		this.rejino = rejino;
	}

	public String getKaiinno() {
		return kaiinno;
	}

	public void setKaiinno(String kaiinno) {
		this.kaiinno = kaiinno;
	}

	@Execute(validator = false)
	public String desc() {
		return "/hazime.jsp";
	}

	/** ログイン画面を表示する実行メソッド */
	@Execute(validator = false)
	public String index() {
		return "index.jsp";
	}

	@Execute(validator = false)
	public String menu() {
		return "/kaiinmenu.jsp";
	}

	@Execute(validator = false)
	public String kai() {
		return "/kaiin/kaiin.jsp";
	}

	@Execute(validator = false)
	public String tou() {
		return "/kaiin/touroku.jsp";
	}

	@Execute(validator = false)
	public String rire() {
		return "/kaiin/rireki.jsp";
	}

	@Execute(validator = false)
	public String kasi() {
		return "/kasidasi/kasidasi.jsp";
	}

	@Execute(validator = false)
	public String hen() {
		return "/henkyaku/henkyaku.jsp";
	}

	@Execute(validator = false)
	public String hurikaeme() {
		return "/hurikaemenu.jsp";
	}

	@Execute(validator = false)
	public String kariken() {
		return "/kaiin/karikaiinkensaku.jsp";
	}

	@Execute(validator = false)
	public String enta() {
		entaiList = kaiinService.findentai(kaiinDto);
		return "/kaiin/entailist.jsp";
	}

	@Execute(validator = false)
	public String entry() {
		return "/kaiin/entaiins.jsp";
	}

	@Execute(validator = false)
	public String black() {
		blackList = kaiinService.black();
		return "/kaiin/Blackitiran.jsp";
	}

	@Execute(validator = false)
	public String elist() {
		entaiList = kaiinService.findentai(kaiinDto);
		return "/kaiin/entai.jsp";
	}

	@Execute(validator = false)
	public String toriokilist() {
		toriokiList = kaiinService.findtorioki();
		return "/kaiin/toriokiitiran.jsp";
	}

	@Execute(validator = false)
	public String hurikaetouhen() {
		return "/hurikae/hurikaeins.jsp";
	}

	@Execute(validator = false)
	public String hurikaesinsei() {
		return "/hurikae/";
	}

	@Execute(validator = false)
	public String hurikaelist() {
		hurikaeList = hurikaeservice.hurikae();
		return "/hurikae/hurikaelist.jsp";
	}

	@Execute(validator = false)
	public String top() {
		return "/hazime.jsp";
	}

	/**
	 * ログインボタンがクリックされた際に呼び出される実行メソッド
	 * 
	 * @throws DatabaseException
	 */
	@Execute(validator = true, input = "login.jsp")
	public String loginconfirm() {
		// ログイン画面で入力されたユーザ名とパスワードでユーザを検索
		System.out.println(loginform.teninno);
		Tenin tenin = teninService.getTenin(loginform.teninno);
		String reji = loginform.rejino;
		System.out.println(reji);
		if (tenin != null) {
			// ログイン成功
			loginDto.setTeninno(tenin.teninno);
			loginDto.setRejino(reji);
			loginDto.setTeninname(tenin.teninname);
			loginDto.setYakushoku(tenin.yakushoku);
			// ログイン後に表示するメニュー画面
			return "hazime.jsp";
		} else {
			// ログイン失敗なのでもう一度ログイン画面に遷移

			return "login.jsp";
		}

	}
}
