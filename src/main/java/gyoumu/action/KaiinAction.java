package gyoumu.action;

import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import gyoumu.dto.HenkouDto;
import gyoumu.dto.KaiinDto;
import gyoumu.entity.Kaiin;
import gyoumu.entity.Kaiin2;
import gyoumu.entity.Kaiin4;
import gyoumu.entity.Kasidasimeisai;
import gyoumu.entity.Kaikei;
import gyoumu.entity.Kaikei2;
import gyoumu.form.KaiinForm;
import gyoumu.service.KaiinService;
import gyoumu.service.KaikeiService;
import gyoumu.service.KarikaiinService;
import gyoumu.dto.KaikeiDto;
import gyoumu.dto.LoginDto;

public class KaiinAction {

	@Resource
	protected HttpServletRequest request;

	@Resource
	protected HttpServletResponse response;

	@Resource
	protected HttpSession session;

	@Resource
	protected ServletContext application;

	public List<Kaiin> kaiinList;
	public String kaiinList2;

	public List<Kaikei2> rirekiList;

	public List<Kaiin4> entaiList;
	
	public List<Kaiin> entaiList2;  

	public List<Kaiin> minouList;

	public List<Kaiin2> kariList;

	public List<Kaiin> blackList;

	@Execute(validator = false)
	public String kai() {
		return "/kaiin/kaiin.jsp";
	}

	@Execute(validator = false)
	public String tou() {
		return "/kaiin/touroku.jsp";
	}

	@Execute(validator = false)
	public String rire() {
		return "/kaiin/rireki.jsp";
	}

	@Resource
	protected KaiinService kaiinService;

	@Resource
	protected KarikaiinService karikaiinService;

	@Resource
	protected KaikeiService kaikeiService;

	@Resource
	@ActionForm
	protected KaiinForm kaiinform;

	@Resource
	protected LoginDto logindto;

	@Resource
	protected KaiinDto kaiinDto;

	@Resource
	protected KaikeiDto kaikeiDto;

	@Resource
	protected HenkouDto henkouDto;

	private String kaiinno = "";

	public String getKaiinno() {
		return kaiinno;
	}

	public void setKaiinno(String kaiinno) {
		this.kaiinno = kaiinno;
	}

	@Execute(validator = false)
	public String kensaku() {
		kaiinDto.setKaiinname(kaiinform.kaiinname);
		kaiinList = kaiinService.findkaiin(kaiinDto);
		return "/kaiin/kaiinkensaku.jsp";
	}

	@Execute(validator = false)
	public String kensaku2() {
		String str = (String) session.getAttribute("kaiinno");
		session.setAttribute("kaiinno", kaiinno);
		String kaiinno = kaiinform.getKaiinno();
		System.out.println("form" + kaiinno);
		kaiinDto.setKaiinno(kaiinno);
		kaiinDto.setKaiinno(kaiinform.kaiinno);
		kaiinList = kaiinService.findkaiin(kaiinDto);
		return "/kaiin/kaiinupd.jsp";
	}

	@Execute(validator = false)
	public String upd() {
		String str = (String) session.getAttribute("kaiinno");
		session.setAttribute("kaiinno", kaiinno);
		String kaiinno = kaiinform.getKaiinno();
		kaiinDto.setKaiinno(kaiinno);
		kaiinDto.setKaiinno(kaiinform.kaiinno);
		kaiinDto.setKaiinname(kaiinform.kaiinname);
		kaiinDto.setKaiinhurigana(kaiinform.kaiinhurigana);
		kaiinDto.setTel(kaiinform.tel);
		kaiinDto.setMail(kaiinform.mail);
		kaiinDto.setAddress(kaiinform.address);
		kaiinDto.setShokugyo(kaiinform.shokugyo);
		kaiinDto.setTaikaiflg(kaiinform.taikaiflg);
		kaiinDto.setBlackflg(kaiinform.blackflg);
		kaiinService.updkaiin(kaiinDto);
		return "/kaiin/kaiinupdZ.jsp";
	}

	@Execute(validator = false)
	public String ins3() {
		kaiinDto.setKarikaiinno(kaiinform.karikaiinno);
		kaiinDto.setKaiinno(kaiinService.findNo(kaiinDto));
		kaiinDto.setKaiinname(kaiinform.kaiinname);
		kaiinDto.setKaiinhurigana(kaiinform.kaiinhurigana);
		kaiinDto.setGender(kaiinform.gender);
		kaiinDto.setTel(kaiinform.tel);
		kaiinDto.setBirthday(kaiinform.birthday);
		kaiinDto.setMail(kaiinform.mail);
		kaiinDto.setAddress(kaiinform.address);
		kaiinDto.setShokugyo(kaiinform.shokugyo);
		kaiinDto.setMibunshokubun(kaiinform.mibunshokubun);
		kaiinDto.setMibunshono(kaiinform.mibunshono);
		kaiinDto.setTaikaiflg("0");
		kaiinDto.setBlackflg("0");
		kaiinDto.setThreeentaicount("0");
		Calendar c = Calendar.getInstance();
		kaiinDto.setHakkouday(c.getTime());
		kaiinService.insert(kaiinDto);

		kaiinList = kaiinService.findkaiin2(kaiinDto);

		System.out.println("かりかいいんなんばー" + kaiinDto.getKarikaiinno());
		karikaiinService.delete(kaiinDto);
		/* 会計追加 */
		kaikeiDto.setKaikeino(kaikeiService.findNo(kaikeiDto));
		kaikeiDto.setKingaku(500);
		kaikeiDto.setKaikeikubun("03");
		kaikeiDto.setKaikeinengappi(c.getTime());
		kaikeiDto.setKaiinno(kaiinDto.getKaiinno());
		kaikeiDto.setKingaku(500);
		kaikeiDto.setHenkinno(null);
		System.out.println("んああああああああああああああ" + logindto.getRejino());
		kaikeiDto.setRejino(logindto.getRejino());
		System.out.println("んあああああああああああああああ" + logindto.getTeninno());
		kaikeiDto.setTeninno(logindto.getTeninno());
		kaikeiService.insert(kaikeiDto);
		return "/kaiin/kaiininsZ.jsp";
	}

	@Execute(validator = false)
	public String ins() {
		/* 会員追加 */
		kaiinDto.setKaiinno(kaiinService.findNo(kaiinDto));
		kaiinDto.setKaiinname(kaiinform.kaiinname);
		kaiinDto.setKaiinhurigana(kaiinform.kaiinhurigana);
		kaiinDto.setGender(kaiinform.gender);
		kaiinDto.setTel(kaiinform.tel);
		kaiinDto.setBirthday(kaiinform.birthday);
		kaiinDto.setMail(kaiinform.mail);
		kaiinDto.setAddress(kaiinform.address);
		kaiinDto.setShokugyo(kaiinform.shokugyo);
		kaiinDto.setMibunshokubun(kaiinform.mibunshokubun);
		kaiinDto.setMibunshono(kaiinform.mibunshono);
		kaiinDto.setTaikaiflg("0");
		kaiinDto.setBlackflg("0");
		kaiinDto.setThreeentaicount("0");
		Calendar c = Calendar.getInstance();
		kaiinDto.setHakkouday(c.getTime());
		kaiinService.insert(kaiinDto);
		kaiinList = kaiinService.findkaiin2(kaiinDto);
		/* 会計追加 */
		kaikeiDto.setKaikeino(kaikeiService.findNo(kaikeiDto));
		kaikeiDto.setKingaku(500);
		kaikeiDto.setKaikeikubun("03");
		kaikeiDto.setKaikeinengappi(c.getTime());
		kaikeiDto.setKaiinno(kaiinDto.getKaiinno());
		kaikeiDto.setKingaku(500);
		kaikeiDto.setHenkinno(null);
		System.out.println(logindto.getRejino());
		kaikeiDto.setRejino(logindto.getRejino());
		System.out.println(logindto.getTeninno());
		kaikeiDto.setTeninno(logindto.getTeninno());
		kaikeiService.insert(kaikeiDto);
		System.out.println(kaiinDto.getKaiinno());
		return "/kaiin/kaiininsZ.jsp";
	}

	@Execute(validator = false)
	public String rireki() {
		kaiinDto.setKaiinno(kaiinform.kaiinno);
		rirekiList = kaiinService.findrireki(kaiinDto);
		return "/kaiin/kasidasilist.jsp";
	}

	@Execute(validator = false)
	public String entry() {
		return "/kaiin/entaiins.jsp";
	}

	@Execute(validator = false)
	public String karikaiinins() {
		kaiinDto.setTel(kaiinform.tel);
		kariList = kaiinService.findkarikaiin(kaiinDto);
		return "/kaiin/karikaiinins.jsp";
	}

	@Execute(validator = false)
	public String enta() {
		entaiList2 = kaiinService.findminou(kaiinDto);
		return "/kaiin/entailist.jsp";
	}

	@Execute(validator = false)
	public String ins2() {
		kaiinDto.setKaiinno(kaiinform.getKaiinno());
		kaiinDto.setMinouentaiken(kaiinform.getMinouentaiken());
		kaiinService.summinou(kaiinDto);
		return "/kaiin/minouentaifin.jsp";
	}

	@Execute(validator = false)
	public String top() {
		return "/hazime.jsp";
	}

}
