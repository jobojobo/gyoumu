/*
 * Copyright 2004-2008 the Seasar Foundation and the Others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package gyoumu.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import gyoumu.dto.HurikaeDto;
import gyoumu.dto.KaiinDto;
import gyoumu.entity.Dvd;
import gyoumu.entity.Dvd3;
import gyoumu.entity.Hurikae;
import gyoumu.entity.Kaiin;
import gyoumu.form.KaiinForm;
import gyoumu.form.hurikaeForm;
import gyoumu.service.HurikaeService;
import gyoumu.service.KaiinService;

public class HurikaeAction {

	@Resource
	protected HurikaeService hurikaeService;
	
	@Resource
	protected HttpServletRequest request;

	@Resource
	protected HttpServletResponse response;

	@Resource
	protected HttpSession session;

	@Resource
	protected ServletContext application;

	public List<Kaiin> kaiinList;
	
	public List<Kaiin> rirekiList;
	
	public List<Kaiin> entaiList;
	public List<Kaiin> blackList;
	public List<Kaiin> toriokiList;

	public List<Dvd3> hurikaeList;
	@Resource
	protected KaiinService kaiinService;
	
	@Resource
	protected HurikaeDto hurikaeDto;
	
	@Resource
	@ActionForm
	protected hurikaeForm hurikaeform;
	
	private String kaiinno = "";

	public String getKaiinno() {
		return kaiinno;
	}

	public void setKaiinno(String kaiinno) {
		this.kaiinno = kaiinno;
	}
	
	@Execute(validator = false)
	public String index() {
		return "index.jsp";
	}

	@Execute(validator = false)
	public String huco() {
		String mozi = "振替申請中";
				String mozi2 = "振替承認";
		hurikaeDto.setDvdno(hurikaeform.dvdno);
		hurikaeDto.setTenpono(hurikaeform.tenpono);
		String dvd = hurikaeService.get(hurikaeDto);
		System.out.println("ああああああああああああああああああああああ"+dvd);
		if(dvd.equals(mozi)){
		hurikaeDto.setKasidasistatus("振替承認");
		System.out.println("振替承認にセットしました");
		}else if(dvd.equals(mozi2)){
		hurikaeDto.setKasidasistatus("貸出可");
		System.out.println("貸出可にセットしました");
		}
		System.out.println("ちんぽっぽ"+hurikaeDto.getDvdno());
		System.out.println("ちんぽ"+hurikaeDto.getTenpono());
		return "/hurikae/hurikaeconfirrm.jsp";
	}
	@Execute(validator = false)
	public String fin() {
		hurikaeService.update(hurikaeDto);
		hurikaeService.update2(hurikaeDto);
		return "/hurikae/hurikaefin.jsp";
	}
	
	@Execute(validator = false)
	public String top() {
		return "/hazime.jsp";
	}
	
	@Execute(validator = false)
	public String hurikaelist() {
		hurikaeList = hurikaeService.hurikae();
		return "/hurikaelist.jsp";
	}
	
}