package gyoumu.service;

import gyoumu.entity.Ryokin;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.RyokinNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Ryokin}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/07/19 9:32:43")
public class RyokinService extends AbstractService<Ryokin> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param hakusu
     *            識別子
     * @param sinsakukubun
     *            識別子
     * @return エンティティ
     */
    public Ryokin findById(String hakusu, String sinsakukubun) {
        return select().id(hakusu, sinsakukubun).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Ryokin> findAllOrderById() {
        return select().orderBy(asc(hakusu()), asc(sinsakukubun())).getResultList();
    }
}