package gyoumu.service;

import gyoumu.entity.Dvd;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.DvdNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Dvd}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/07/19 9:32:43")
public class DvdService extends AbstractService<Dvd> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param dvdno
     *            識別子
     * @return エンティティ
     */
    public Dvd findById(String dvdno) {
        return select().id(dvdno).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Dvd> findAllOrderById() {
        return select().orderBy(asc(dvdno())).getResultList();
    }
}