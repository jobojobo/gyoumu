package gyoumu.service;

import gyoumu.entity.Sakuhin;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.SakuhinNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Sakuhin}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/07/19 9:32:43")
public class SakuhinService extends AbstractService<Sakuhin> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param sakuhinno
     *            識別子
     * @return エンティティ
     */
    public Sakuhin findById(String sakuhinno) {
        return select().id(sakuhinno).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Sakuhin> findAllOrderById() {
        return select().orderBy(asc(sakuhinno())).getResultList();
    }
}