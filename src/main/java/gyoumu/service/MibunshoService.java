package gyoumu.service;

import gyoumu.entity.Mibunsho;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.MibunshoNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Mibunsho}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/07/19 9:32:43")
public class MibunshoService extends AbstractService<Mibunsho> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param mibunshokubun
     *            識別子
     * @return エンティティ
     */
    public Mibunsho findById(String mibunshokubun) {
        return select().id(mibunshokubun).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Mibunsho> findAllOrderById() {
        return select().orderBy(asc(mibunshokubun())).getResultList();
    }
}