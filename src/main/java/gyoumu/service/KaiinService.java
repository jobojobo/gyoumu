package gyoumu.service;

import gyoumu.dto.KaiinDto;
import gyoumu.entity.Dvd;
import gyoumu.entity.Dvd2;
import gyoumu.entity.Kaiin;
import gyoumu.entity.Kaiin2;
import gyoumu.entity.Kaiin4;
import gyoumu.entity.Kaikei;
import gyoumu.entity.Kaikei2;
import gyoumu.entity.Kasidasimeisai;

import java.math.BigInteger;
import java.util.List;
import javax.annotation.Generated;

import org.seasar.framework.beans.util.Beans;

import static gyoumu.entity.KaiinNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Kaiin}のサービスクラスです。
 * 
 */
@Generated(value = { "S2JDBC-Gen 2.4.46",
		"org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl" }, date = "2017/07/19 9:32:43")
public class KaiinService extends AbstractService<Kaiin> {

	/**
	 * 識別子でエンティティを検索します。
	 * 
	 * @param kaiinno
	 *            識別子
	 * @return エンティティ
	 */
	public Kaiin findById(String kaiinno) {
		return select().id(kaiinno).getSingleResult();
	}

	/**
	 * 識別子の昇順ですべてのエンティティを検索します。
	 * 
	 * @return エンティティのリスト
	 */
	public List<Kaiin> findAllOrderById() {
		return select().orderBy(asc(kaiinno())).getResultList();
	}

	public List<Kaiin> findkaiin(KaiinDto kaiinDto) {
		List<Kaiin> kaiinList = null;
		String no = kaiinDto.getKaiinname();
		System.out.println(no);
		kaiinList = jdbcManager.selectBySql(Kaiin.class, "SELECT * FROM kaiin WHERE kaiinname = ? OR kaiinhurigana = ?", no,no).getResultList();
		return kaiinList;
	}
	
	public List<Kaiin> findkaiin2(KaiinDto kaiinDto) {
		List<Kaiin> kaiinList = null;
		String no = kaiinDto.getKaiinno();
		System.out.println(no);
		kaiinList = jdbcManager.selectBySql(Kaiin.class, "SELECT kaiinno FROM kaiin WHERE kaiinno = ?", no).getResultList();
		return kaiinList;
	}

	public int updkaiin(KaiinDto kaiinDto) {
		String no = kaiinDto.getKaiinno();
		Kaiin kaiin = jdbcManager.selectBySql(Kaiin.class, "SELECT * FROM kaiin WHERE kaiinno = ?", no)
				.getSingleResult();
		if (!kaiinDto.getKaiinname().equals("")) {
			kaiin.kaiinname = kaiinDto.getKaiinname();
		}
		if (!kaiinDto.getKaiinhurigana().equals("")) {
			kaiin.kaiinhurigana = kaiinDto.getKaiinhurigana();
		}
		if (!kaiinDto.getTel().equals("")) {
			kaiin.tel = kaiinDto.getTel();
		}
		if (!kaiinDto.getMail().equals("")) {
			kaiin.mail = kaiinDto.getMail();
		}
		if (!kaiinDto.getAddress().equals("")) {
			kaiin.address = kaiinDto.getAddress();
		}
		if (!kaiinDto.getShokugyo().equals("")) {
			kaiin.shokugyo = kaiinDto.getShokugyo();
		}
		if (!kaiinDto.getTaikaiflg().equals("")) {
			kaiin.taikaiflg = kaiinDto.getTaikaiflg();
		}
		if (!kaiinDto.getBlackflg().equals("")) {
			kaiin.blackflg = kaiinDto.getBlackflg();
		}
		return jdbcManager.update(kaiin)
				.includes("kaiinname", "kaiinhurigana", "tel", "mail", "address", "shokugyo", "taikaiflg", "blackflg")
				.execute();
	}

	public String findNo(KaiinDto kaiinDto) {
		Integer no = jdbcManager.selectBySql(Integer.class, "select count(*) from kaiin").getSingleResult();
		return String.format("%06d", no + 1);
	}

	public int insert(KaiinDto kaiinDto) {
		Kaiin Kaiin = Beans.createAndCopy(Kaiin.class, kaiinDto).dateConverter("yyyy/MM/dd", "birthday")
				.dateConverter("yyyy/MM/dd", "hakkouday").execute();
		return jdbcManager.insert(Kaiin)
				.includes("kaiinno", "kaiinname", "kaiinhurigana", "gender", "tel", "birthday", "mail", "address",
						"shokugyo", "mibunshokubun", "mibunshono", "taikaiflg", "blackflg", "threeentaicount",
						"hakkouday")
				.execute();
	}

	public int summinou(KaiinDto kaiinDto) {
		String no = kaiinDto.getKaiinno();
		BigInteger kane = kaiinDto.getMinouentaiken();
		
		int kane2 = jdbcManager.selectBySql(int.class, "select minouentaiken + ? FROM kaiin where kaiinno = ?",kane,no)
				.getSingleResult();
		System.out.println(kane2);
        String sql = "UPDATE kaiin SET minouentaiken= " + kane2 + " WHERE kaiinno= "
                + no;
        int result = jdbcManager.updateBySql(sql).execute();
		return result;
	}

	public int update4(KaiinDto kaiinDto) {
		String no = kaiinDto.getKaiinno();
		BigInteger kane = kaiinDto.getMinouentaiken();
		
		Kaiin Kaiin = jdbcManager.selectBySql(Kaiin.class, "select minouentaiken + ? FROM kaiin where kaiinno = ?",kane,no)
				.getSingleResult();
		
		if (!kaiinDto.getMinouentaiken().equals("")) {
			Kaiin.minouentaiken = kaiinDto.getMinouentaiken();
		}
		return jdbcManager.update(Kaiin).includes("minouentaiken").execute();
	}
	public List<Kaikei2> findrireki(KaiinDto kaiinDto) {
		List<Kaikei2> rirekiList = null;
		String no = kaiinDto.getKaiinno();
		System.out.println("会員番号" + no);

		System.out.println("ああああああああああああああああああああああああああああああああ");
		rirekiList = jdbcManager.selectBySql(Kaikei2.class,
				"SELECT m.kaikeino,m.kasidasimeisaino,m.dvdno,m.henkyakuday,m.henkyakuyoteiday,m.hakusu,m.tanka,m.waribikino,k.kaikeikubun,k.kaikeinengappi,k.kingaku,r.tenpono,k.rejino,k.teninno,k.kaiinno,k.henkinno  FROM kaikei k inner join kasidasimeisai m on m.kaikeino = k.kaikeino inner join reji r on k.rejino = r.rejino WHERE k.kaiinno = ? AND kasidasi",
				no).getResultList();
		System.out.println("いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい"+rirekiList);
		return rirekiList;
	}

	public List<Kaiin4> findentai(KaiinDto kaiinDto) {
		List<Kaiin4> entaiList = null;
		entaiList = jdbcManager
				.selectBySql(Kaiin4.class,
						"SELECT k.kaiinno,k.kaiinname,k.tel,s.title,m.henkyakuyoteiday,t.tenponame FROM kaiin k join kaikei kk on kk.kaiinno = k.kaiinno join kasidasimeisai m on kk.kaikeino = m.kaikeino join dvd d on m.dvdno = d.dvdno join sakuhin s on d.sakuhinno = s.sakuhinno join reji r on r.rejino = kk.rejino join tenpo t on r.tenpono = t.tenpono WHERE m.henkyakuday IS NULL AND henkyakuyoteiday < TRUNC(SYSDATE) ")
				.getResultList();
		return entaiList;
	}

	public List<Kaiin> findminou(KaiinDto kaiinDto) {
		List<Kaiin> entaiList = null;
		entaiList = jdbcManager
				.selectBySql(Kaiin.class,
						"SELECT k.kaiinno,k.kaiinname,k.tel,s.title,k.minouentaiken,m.henkyakuyoteiday,t.tenponame FROM kaiin k join kaikei kk on kk.kaiinno = k.kaiinno join kasidasimeisai m on kk.kaikeino = m.kaikeino join dvd d on m.dvdno = d.dvdno join sakuhin s on d.sakuhinno = s.sakuhinno join reji r on r.rejino = kk.rejino join tenpo t on r.tenpono = t.tenpono WHERE m.henkyakuday IS NULL AND henkyakuyoteiday < TRUNC(SYSDATE) AND k.minouentaiken != 0")
				.getResultList();
		return entaiList;
	}

	public List<Kaiin2> findkarikaiin(KaiinDto kaiinDto) {
		List<Kaiin2> kariList = null;
		String no = kaiinDto.getTel();
		System.out.println(no);
		kariList = jdbcManager.selectBySql(Kaiin2.class,
				"SELECT karikaiinno,kaiinname,kaiinhurigana,gender,address,tel,mail,birthday,shokugyo FROM karikaiin WHERE tel = ?",
				no).getResultList();
		return kariList;
	}

	public List<Kaiin> black() {
		List<Kaiin> blackList = null;
		blackList = jdbcManager.selectBySql(Kaiin.class, "SELECT * FROM kaiin WHERE blackflg != 0").getResultList();
		return blackList;
	}

	public List<Dvd2> findtorioki() {
		List<Dvd2> toriokiList = null;
		toriokiList = jdbcManager
				.selectBySql(Dvd2.class,
						"SELECT * FROM torioki k join dvd d on k.dvdno = d.dvdno join sakuhin s on s.sakuhinno = d.sakuhinno WHERE d.kasidasistatus = '取り置き済'")
				.getResultList();
		return toriokiList;
	}


}