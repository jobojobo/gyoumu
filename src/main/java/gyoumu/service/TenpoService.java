package gyoumu.service;

import gyoumu.entity.Tenpo;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.TenpoNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Tenpo}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/07/19 9:32:43")
public class TenpoService extends AbstractService<Tenpo> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param tenpono
     *            識別子
     * @return エンティティ
     */
    public Tenpo findById(String tenpono) {
        return select().id(tenpono).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Tenpo> findAllOrderById() {
        return select().orderBy(asc(tenpono())).getResultList();
    }
}