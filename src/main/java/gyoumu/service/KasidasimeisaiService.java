package gyoumu.service;

import gyoumu.entity.Kasidasimeisai;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.KasidasimeisaiNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Kasidasimeisai}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/07/19 9:32:43")
public class KasidasimeisaiService extends AbstractService<Kasidasimeisai> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param kaikeino
     *            識別子
     * @param kasidasimeisaino
     *            識別子
     * @return エンティティ
     */
    public Kasidasimeisai findById(String kaikeino, String kasidasimeisaino) {
        return select().id(kaikeino, kasidasimeisaino).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Kasidasimeisai> findAllOrderById() {
        return select().orderBy(asc(kaikeino()), asc(kasidasimeisaino())).getResultList();
    }
}