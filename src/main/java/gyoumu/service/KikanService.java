package gyoumu.service;

import gyoumu.entity.Kikan;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.KikanNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Kikan}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/07/19 9:32:43")
public class KikanService extends AbstractService<Kikan> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param hakusu
     *            識別子
     * @return エンティティ
     */
    public Kikan findById(String hakusu) {
        return select().id(hakusu).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Kikan> findAllOrderById() {
        return select().orderBy(asc(hakusu())).getResultList();
    }
}