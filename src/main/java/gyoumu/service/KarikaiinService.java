package gyoumu.service;

import gyoumu.dto.KaiinDto;
import gyoumu.entity.Karikaiin;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.KarikaiinNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Karikaiin}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/07/19 9:32:43")
public class KarikaiinService extends AbstractService<Karikaiin> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param karikaiinno
     *            識別子
     * @return エンティティ
     */
    public Karikaiin findById(String karikaiinno) {
        return select().id(karikaiinno).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Karikaiin> findAllOrderById() {
        return select().orderBy(asc(karikaiinno())).getResultList();
    }
    
	public int delete(KaiinDto kaiinDto) {
		Karikaiin karikaiin = jdbcManager.from(Karikaiin.class).id(kaiinDto.getKarikaiinno()).getSingleResult();
		return jdbcManager.delete(karikaiin).execute();
	}
}