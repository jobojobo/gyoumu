package gyoumu.service;

import gyoumu.dto.HurikaeDto;
import gyoumu.entity.Dvd;
import gyoumu.entity.Hurikae;
import gyoumu.entity.Dvd3;

import java.util.Date;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.HurikaeNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Hurikae}のサービスクラスです。
 * 
 */
@Generated(value = { "S2JDBC-Gen 2.4.46",
		"org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl" }, date = "2017/07/19 9:32:43")
public class HurikaeService extends AbstractService<Hurikae> {

	/**
	 * 識別子でエンティティを検索します。
	 * 
	 * @param tenpono
	 *            識別子
	 * @param sinseiday
	 *            識別子
	 * @param dvdno
	 *            識別子
	 * @return エンティティ
	 */
	public Hurikae findById(String tenpono, Date sinseiday, String dvdno) {
		return select().id(tenpono, sinseiday, dvdno).getSingleResult();
	}

	/**
	 * 識別子の昇順ですべてのエンティティを検索します。
	 * 
	 * @return エンティティのリスト
	 */
	public List<Hurikae> findAllOrderById() {
		return select().orderBy(asc(tenpono()), asc(sinseiday()), asc(dvdno())).getResultList();
	}

	public int update(HurikaeDto hurikaeDto) {
		String no = hurikaeDto.getDvdno();
		Dvd dvd = jdbcManager.selectBySql(Dvd.class, "SELECT * FROM dvd WHERE dvdno = ?", no).getSingleResult();
		if (!hurikaeDto.getTenpono().equals("")) {
			dvd.tenpono = hurikaeDto.getTenpono();
		}
		System.out.println("あああああああああああああああああああああああああ");
		return jdbcManager.update(dvd).includes("tenpono").execute();

	}

	public int update2(HurikaeDto hurikaeDto) {
		Dvd dvd = jdbcManager.from(Dvd.class).id(hurikaeDto.getDvdno()).getSingleResult();
		System.out.println(hurikaeDto.getKasidasistatus());
		dvd.kasidasistatus = hurikaeDto.getKasidasistatus();
		jdbcManager.update(dvd).execute();
			System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn");
			/*	return jdbcManager.update(kasista).includes("kasidasistatus").execute(); */
		return 0;

	}
	public String get(HurikaeDto hurikaeDto) {
		String dvd = null;
		String sa = hurikaeDto.getDvdno();
		dvd = jdbcManager.selectBySql(String.class,
				"SELECT kasidasistatus FROM dvd WHERE dvdno = ?",
				sa).getSingleResult();
		return dvd;
	}
	
	public List<Dvd3> hurikae() {
		List<Dvd3> hurikaeList = jdbcManager.selectBySql(Dvd3.class,
				"SELECT d.dvdno,s.title,d.kasidasistatus FROM sakuhin s inner join dvd d on s.sakuhinno=d.sakuhinno WHERE d.kasidasistatus = '振替承認' OR d.kasidasistatus = '振替申請中'"
).getResultList();
		return hurikaeList;
	}
	
}