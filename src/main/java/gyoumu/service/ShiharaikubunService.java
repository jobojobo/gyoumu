package gyoumu.service;

import gyoumu.entity.Shiharaikubun;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.ShiharaikubunNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Shiharaikubun}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/08/31 15:52:52")
public class ShiharaikubunService extends AbstractService<Shiharaikubun> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param kaikeikubun
     *            識別子
     * @return エンティティ
     */
    public Shiharaikubun findById(String kaikeikubun) {
        return select().id(kaikeikubun).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Shiharaikubun> findAllOrderById() {
        return select().orderBy(asc(kaikeikubun())).getResultList();
    }
}