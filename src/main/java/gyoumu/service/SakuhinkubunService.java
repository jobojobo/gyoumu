package gyoumu.service;

import gyoumu.entity.Sakuhinkubun;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.SakuhinkubunNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Sakuhinkubun}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/07/19 9:32:43")
public class SakuhinkubunService extends AbstractService<Sakuhinkubun> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param sinsakukubun
     *            識別子
     * @return エンティティ
     */
    public Sakuhinkubun findById(String sinsakukubun) {
        return select().id(sinsakukubun).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Sakuhinkubun> findAllOrderById() {
        return select().orderBy(asc(sinsakukubun())).getResultList();
    }
}