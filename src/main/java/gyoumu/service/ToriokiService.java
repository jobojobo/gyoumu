package gyoumu.service;

import gyoumu.entity.Torioki;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.ToriokiNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Torioki}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/07/19 9:32:43")
public class ToriokiService extends AbstractService<Torioki> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param sakuhinno
     *            識別子
     * @param tel
     *            識別子
     * @return エンティティ
     */
    public Torioki findById(String sakuhinno, String tel) {
        return select().id(sakuhinno, tel).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Torioki> findAllOrderById() {
        return select().orderBy(asc(sakuhinno()), asc(tel())).getResultList();
    }
}