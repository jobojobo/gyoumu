package gyoumu.service;

import gyoumu.entity.Tenin;

import java.util.List;
import javax.annotation.Generated;

import org.seasar.extension.jdbc.where.SimpleWhere;
import org.seasar.framework.beans.util.Beans;

import static gyoumu.entity.TeninNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Tenin}のサービスクラスです。
 * 
 */
@Generated(value = { "S2JDBC-Gen 2.4.46",
		"org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl" }, date = "2017/07/05 14:22:36")
public class TeninService extends AbstractService<Tenin> {

	/**
	 * 識別子でエンティティを検索します。
	 * 
	 * @param teninno
	 *            識別子
	 * @return エンティティ
	 */
	public Tenin findById(String teninno) {
		return select().id(teninno).getSingleResult();
	}

	/**
	 * 識別子の昇順ですべてのエンティティを検索します。
	 * 
	 * @return エンティティのリスト
	 */
	public List<Tenin> findAllOrderById() {
		return select().orderBy(asc(teninno())).getResultList();
	}

	/* ログイン用 */
	public Tenin getTenin(String teninno){
		SimpleWhere where = new SimpleWhere();
		where.eq("teninno", teninno);

		return select().where(where).getSingleResult();
	}
	/* 店員名 */
    public String findId(String teninno) {
		String name = jdbcManager.selectBySql(String.class, "select teninname from tenin WHERE teninno = ?",teninno).getSingleResult();
		return name;
	}
}