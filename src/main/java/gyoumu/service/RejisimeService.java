package gyoumu.service;

import gyoumu.entity.Rejisime;
import java.util.Date;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.RejisimeNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Rejisime}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/07/19 9:32:43")
public class RejisimeService extends AbstractService<Rejisime> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param rejino
     *            識別子
     * @param rejisimeday
     *            識別子
     * @return エンティティ
     */
    public Rejisime findById(String rejino, Date rejisimeday) {
        return select().id(rejino, rejisimeday).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Rejisime> findAllOrderById() {
        return select().orderBy(asc(rejino()), asc(rejisimeday())).getResultList();
    }
}