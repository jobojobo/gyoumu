package gyoumu.service;

import gyoumu.entity.Waribiki;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.WaribikiNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Waribiki}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/07/19 9:32:43")
public class WaribikiService extends AbstractService<Waribiki> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param waribikino
     *            識別子
     * @return エンティティ
     */
    public Waribiki findById(String waribikino) {
        return select().id(waribikino).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Waribiki> findAllOrderById() {
        return select().orderBy(asc(waribikino())).getResultList();
    }
}