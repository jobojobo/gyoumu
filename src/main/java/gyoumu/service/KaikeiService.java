package gyoumu.service;

import org.seasar.framework.beans.util.Beans;

import gyoumu.entity.Kaikei;
import gyoumu.dto.KaikeiDto;

import static gyoumu.entity.KaikeiNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

import java.util.List;

import javax.annotation.Generated;

/**
 * {@link Kaikei}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/07/13 15:02:41")
public class KaikeiService extends AbstractService<Kaikei> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param kaikeino
     *            識別子
     * @return エンティティ
     */
    public Kaikei findById(String kaikeino) {
        return select().id(kaikeino).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Kaikei> findAllOrderById() {
        return select().orderBy(asc(kaikeino())).getResultList();
    }
    
    /* 会計登録 */
	public int insert(KaikeiDto kaikeiDto) {
		Kaikei kaikei = Beans.createAndCopy(Kaikei.class, kaikeiDto).execute();
		return jdbcManager.insert(kaikei).execute();
	}
    /* 自動採番 */
    public String findNo(KaikeiDto kaikeiDto) {
		Integer no = jdbcManager.selectBySql(Integer.class, "select max(kaikeino) from kaikei").getSingleResult();
		return String.format("%010d", no + 1);
	}
}