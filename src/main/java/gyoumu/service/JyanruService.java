package gyoumu.service;

import gyoumu.entity.Jyanru;
import java.util.List;
import javax.annotation.Generated;

import static gyoumu.entity.JyanruNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Jyanru}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2017/07/19 9:32:43")
public class JyanruService extends AbstractService<Jyanru> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param jyanruno
     *            識別子
     * @return エンティティ
     */
    public Jyanru findById(String jyanruno) {
        return select().id(jyanruno).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Jyanru> findAllOrderById() {
        return select().orderBy(asc(jyanruno())).getResultList();
    }
}