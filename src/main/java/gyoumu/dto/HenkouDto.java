package gyoumu.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class HenkouDto implements Serializable {

	private static final long serialVersionUID = 1L;

	public String getKaiinname() {
		return kaiinname;
	}

	public void setKaiinname(String kaiinname) {
		this.kaiinname = kaiinname;
	}

	public String getKaiinhurigana() {
		return kaiinhurigana;
	}

	public void setKaiinhurigana(String kaiinhurigana) {
		this.kaiinhurigana = kaiinhurigana;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getShokugyo() {
		return shokugyo;
	}

	public void setShokugyo(String shokugyo) {
		this.shokugyo = shokugyo;
	}

	public String getMibunshokubun() {
		return mibunshokubun;
	}

	public void setMibunshokubun(String mibunshokubun) {
		this.mibunshokubun = mibunshokubun;
	}

	public String getMibunshono() {
		return mibunshono;
	}

	public void setMibunshono(String mibunshono) {
		this.mibunshono = mibunshono;
	}

	private String kaiinname;
	private String kaiinhurigana;
	private String address;

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	private String gender;
	private String tel;
	private String mail;
	private String birthday;
	private String shokugyo;
	private String mibunshokubun;
	private String mibunshono;
	
	private String kaiinno;
	public String getKaiinno() {
		return kaiinno;
	}

	public void setKaiinno(String kaiinno) {
		this.kaiinno = kaiinno;
	}

	public Date getHakkouday() {
		return hakkouday;
	}

	public void setHakkouday(Date hakkouday) {
		this.hakkouday = hakkouday;
	}

	public String getTaikaiflg() {
		return taikaiflg;
	}

	public void setTaikaiflg(String taikaiflg) {
		this.taikaiflg = taikaiflg;
	}

	public String getBlackflg() {
		return blackflg;
	}

	public void setBlackflg(String blackflg) {
		this.blackflg = blackflg;
	}

	public String getThreeentaicount() {
		return threeentaicount;
	}

	public void setThreeentaicount(String threeentaicount) {
		this.threeentaicount = threeentaicount;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private Date hakkouday;
	private String taikaiflg;
	private String blackflg;
	private String threeentaicount;
}
