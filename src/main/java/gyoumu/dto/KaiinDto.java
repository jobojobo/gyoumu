package gyoumu.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class KaiinDto implements Serializable {
	private static final long serialVersionUID = 1L;

	public String getKaiinno() {
		return kaiinno;
	}
	public void setKaiinno(String kaiinno) {
		this.kaiinno = kaiinno;
	}
	public String getKaiinname() {
		return kaiinname;
	}
	public void setKaiinname(String kaiinname) {
		this.kaiinname = kaiinname;
	}
	public String getKaiinhurigana() {
		return kaiinhurigana;
	}
	public void setKaiinhurigana(String kaiinhurigana) {
		this.kaiinhurigana = kaiinhurigana;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getShokugyo() {
		return shokugyo;
	}
	public void setShokugyo(String shokugyo) {
		this.shokugyo = shokugyo;
	}
	public String getMibunshokubun() {
		return mibunshokubun;
	}
	public void setMibunshokubun(String mibunshokubun) {
		this.mibunshokubun = mibunshokubun;
	}
	public String getMibunshono() {
		return mibunshono;
	}
	public void setMibunshono(String mibunshono) {
		this.mibunshono = mibunshono;
	}
	public String getTaikaiflg() {
		return taikaiflg;
	}
	public void setTaikaiflg(String taikaiflg) {
		this.taikaiflg = taikaiflg;
	}
	public Date getHakkouday() {
		return hakkouday;
	}
	public void setHakkouday(Date hakkouday) {
		this.hakkouday = hakkouday;
	}
	public String getBlackflg() {
		return blackflg;
	}
	public void setBlackflg(String blackflg) {
		this.blackflg = blackflg;
	}
	public String getThreeentaicount() {
		return threeentaicount;
	}
	public void setThreeentaicount(String threeentaicount) {
		this.threeentaicount = threeentaicount;
	}
	public BigInteger getMinouentaiken() {
		return minouentaiken;
	}
	public void setMinouentaiken(BigInteger minouentaiken) {
		this.minouentaiken = minouentaiken;
	}
	private String kaiinno;
	private String kaiinname;
	private String kaiinhurigana;
	private String gender;
	private String address;
	private String tel;
	private String mail;
	private String birthday;
	private String shokugyo;
	private String mibunshokubun;
	private String mibunshono;
	private String taikaiflg;
	private Date hakkouday;
	private String blackflg;
	private String threeentaicount;
	private BigInteger minouentaiken;
	
	public String getKaikeino() {
		return kaikeino;
	}
	public void setKaikeino(String kaikeino) {
		this.kaikeino = kaikeino;
	}
	public String getKaikeikubun() {
		return kaikeikubun;
	}
	public void setKaikeikubun(String kaikeikubun) {
		this.kaikeikubun = kaikeikubun;
	}
	public String getKaikeinengappi() {
		return kaikeinengappi;
	}
	public void setKaikeinengappi(String kaikeinengappi) {
		this.kaikeinengappi = kaikeinengappi;
	}
	public String getKingaku() {
		return kingaku;
	}
	public void setKingaku(String kingaku) {
		this.kingaku = kingaku;
	}
	public String getHenkinno() {
		return henkinno;
	}
	public void setHenkinno(String henkinno) {
		this.henkinno = henkinno;
	}
	public String getTenpono() {
		return tenpono;
	}
	public void setTenpono(String tenpono) {
		this.tenpono = tenpono;
	}
	public String getKasidasimeisaino() {
		return kasidasimeisaino;
	}
	public void setKasidasimeisaino(String kasidasimeisaino) {
		this.kasidasimeisaino = kasidasimeisaino;
	}
	public String getDvdno() {
		return dvdno;
	}
	public void setDvdno(String dvdno) {
		this.dvdno = dvdno;
	}
	public String getHenkyakuday() {
		return henkyakuday;
	}
	public void setHenkyakuday(String henkyakuday) {
		this.henkyakuday = henkyakuday;
	}
	public String getHenkyakuyoteiday() {
		return henkyakuyoteiday;
	}
	public void setHenkyakuyoteiday(String henkyakuyoteiday) {
		this.henkyakuyoteiday = henkyakuyoteiday;
	}
	public String getHakusu() {
		return hakusu;
	}
	public void setHakusu(String hakusu) {
		this.hakusu = hakusu;
	}
	public String getWaribikino() {
		return waribikino;
	}
	public void setWaribikino(String waribikino) {
		this.waribikino = waribikino;
	}
	public String getTanka() {
		return tanka;
	}
	public void setTanka(String tanka) {
		this.tanka = tanka;
	}
	private String kaikeino;
	private String kaikeikubun;
	private String kaikeinengappi;
	private String kingaku;
	private String rejino;
	private String teninno;
	private String henkinno;
	private String tenpono;
	private String kasidasimeisaino;
	private String dvdno;
	private String henkyakuday;
	private String henkyakuyoteiday;
	private String hakusu;
	private String waribikino;
	private String tanka;
	private String karikaiinno;

	public String getKarikaiinno() {
		return karikaiinno;
	}
	public void setKarikaiinno(String karikaiinno) {
		this.karikaiinno = karikaiinno;
	}
}
