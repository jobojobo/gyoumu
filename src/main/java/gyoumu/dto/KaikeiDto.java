package gyoumu.dto;

import java.io.Serializable;
import java.util.Date;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class KaikeiDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private String kaikeino;
	private String kaikeikubun;
	private Date kaikeinengappi;
	private int kingaku;
	private String rejino;
	private String teninno;
	private String kaiinno;
	private String henkinno;

	public String getKaikeino() {
		return kaikeino;
	}

	public void setKaikeino(String kaikeino) {
		this.kaikeino = kaikeino;
	}

	public String getKaikeikubun() {
		return kaikeikubun;
	}

	public void setKaikeikubun(String kaikeikubun) {
		this.kaikeikubun = kaikeikubun;
	}

	public int getKingaku() {
		return kingaku;
	}

	public void setKingaku(int kingaku) {
		this.kingaku = kingaku;
	}

	public String getRejino() {
		return rejino;
	}

	public void setRejino(String rejino) {
		this.rejino = rejino;
	}

	public String getTeninno() {
		return teninno;
	}

	public void setTeninno(String teninno) {
		this.teninno = teninno;
	}

	public String getKaiinno() {
		return kaiinno;
	}

	public void setKaiinno(String kaiinno) {
		this.kaiinno = kaiinno;
	}

	public String getHenkinno() {
		return henkinno;
	}

	public void setHenkinno(String henkinno) {
		this.henkinno = henkinno;
	}

	public Date getKaikeinengappi() {
		return kaikeinengappi;
	}

	public void setKaikeinengappi(Date kaikeinengappi) {
		this.kaikeinengappi = kaikeinengappi;
	}

}
