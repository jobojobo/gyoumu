package gyoumu.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class HurikaeDto implements Serializable {
	private static final long serialVersionUID = 1L;

	public String getDvdno() {
		return dvdno;
	}

	public void setDvdno(String dvdno) {
		this.dvdno = dvdno;
	}

	public String getTenpono() {
		return tenpono;
	}

	public void setTenpono(String tenpono) {
		this.tenpono = tenpono;
	}

	public String getKasidasistatus() {
		return kasidasistatus;
	}

	public void setKasidasistatus(String kasidasistatus) {
		this.kasidasistatus = kasidasistatus;
	}

	private String kasidasistatus;
	
	private String dvdno;
	private String tenpono;

}
