package gyoumu.dto;

import java.io.Serializable;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class LoginDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private String teninno;
	private String teninname;
	private String yakushoku;
	private String rejino;
	
	public String getRejino() {
		return rejino;
	}

	public void setRejino(String rejino) {
		this.rejino = rejino;
	}

	public String getTeninno() {
		return teninno;
	}

	public void setTeninno(String teninno) {
		this.teninno = teninno;
	}

	public String getTeninname() {
		return teninname;
	}

	public void setTeninname(String teninname) {
		this.teninname = teninname;
	}

	public String getYakushoku() {
		return yakushoku;
	}

	public void setYakushoku(String yakushoku) {
		this.yakushoku = yakushoku;
	}

}
