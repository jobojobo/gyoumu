package gyoumu.form;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;
import org.seasar.struts.annotation.DateType;
import org.seasar.struts.annotation.Maxlength;
import org.seasar.struts.annotation.Required;

@Component(instance = InstanceType.SESSION)
public class KaiinForm implements Serializable {

	private static final long serialVersionUID = 1L;

	public String getKaiinno() {
		return kaiinno;
	}

	public void setKaiinno(String kaiinno) {
		this.kaiinno = kaiinno;
	}

	public String getKaiinname() {
		return kaiinname;
	}

	public void setKaiinname(String kaiinname) {
		this.kaiinname = kaiinname;
	}

	public String getKaiinhurigana() {
		return kaiinhurigana;
	}

	public void setKaiinhurigana(String kaiinhurigana) {
		this.kaiinhurigana = kaiinhurigana;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getShokugyo() {
		return shokugyo;
	}

	public void setShokugyo(String shokugyo) {
		this.shokugyo = shokugyo;
	}

	public String getMibunshokubun() {
		return mibunshokubun;
	}

	public void setMibunshokubun(String mibunshokubun) {
		this.mibunshokubun = mibunshokubun;
	}

	public String getMibunshono() {
		return mibunshono;
	}

	public void setMibunshono(String mibunshono) {
		this.mibunshono = mibunshono;
	}

	public String getTaikaiflg() {
		return taikaiflg;
	}

	public void setTaikaiflg(String taikaiflg) {
		this.taikaiflg = taikaiflg;
	}

	public String getHakkouday() {
		return hakkouday;
	}

	public void setHakkouday(String hakkouday) {
		this.hakkouday = hakkouday;
	}

	public String getBlackflg() {
		return blackflg;
	}

	public void setBlackflg(String blackflg) {
		this.blackflg = blackflg;
	}

	public String getThreeentaicount() {
		return threeentaicount;
	}

	public void setThreeentaicount(String threeentaicount) {
		this.threeentaicount = threeentaicount;
	}

	public BigInteger getMinouentaiken() {
		return minouentaiken;
	}

	public void setMinouentaiken(BigInteger minouentaiken) {
		this.minouentaiken = minouentaiken;
	}

	public String getKaikeikubun() {
		return kaikeikubun;
	}

	public void setKaikeikubun(String kaikeikubun) {
		this.kaikeikubun = kaikeikubun;
	}

	public String getKaikeinengappi() {
		return kaikeinengappi;
	}

	public void setKaikeinengappi(String kaikeinengappi) {
		this.kaikeinengappi = kaikeinengappi;
	}

	public String getKingaku() {
		return kingaku;
	}

	public void setKingaku(String kingaku) {
		this.kingaku = kingaku;
	}

	public String getKaikeino() {
		return kaikeino;
	}

	public void setKaikeino(String kaikeino) {
		this.kaikeino = kaikeino;
	}

	public String getTenpono() {
		return tenpono;
	}

	public void setTenpono(String tenpono) {
		this.tenpono = tenpono;
	}

	public String getHenkinno() {
		return henkinno;
	}

	public void setHenkinno(String henkinno) {
		this.henkinno = henkinno;
	}

	public String getDvdno() {
		return dvdno;
	}

	public void setDvdno(String dvdno) {
		this.dvdno = dvdno;
	}

	public String getHenkyakuday() {
		return henkyakuday;
	}

	public void setHenkyakuday(String henkyakuday) {
		this.henkyakuday = henkyakuday;
	}

	public String getHenkyakuyoteiday() {
		return henkyakuyoteiday;
	}

	public void setHenkyakuyoteiday(String henkyakuyoteiday) {
		this.henkyakuyoteiday = henkyakuyoteiday;
	}

	public String getHakusu() {
		return hakusu;
	}

	public void setHakusu(String hakusu) {
		this.hakusu = hakusu;
	}

	public String getTanka() {
		return tanka;
	}

	public void setTanka(String tanka) {
		this.tanka = tanka;
	}

	public String getWaribikino() {
		return waribikino;
	}

	public void setWaribikino(String waribikino) {
		this.waribikino = waribikino;
	}

	public String getKarikaiinno() {
		return karikaiinno;
	}

	public void setKarikaiinno(String karikaiinno) {
		this.karikaiinno = karikaiinno;
	}

	@Maxlength(maxlength = 10)
	public String kaikeikubun;
	@Maxlength(maxlength = 10)
	public String kaikeinengappi;

	@Maxlength(maxlength = 10)
	public String kingaku;
	@Maxlength(maxlength = 10)
	public String kaikeino;
	@Maxlength(maxlength = 10)
	public String tenpono;
	@Maxlength(maxlength = 10)
	public String henkinno;
	@Maxlength(maxlength = 10)
	public String dvdno;
	@Maxlength(maxlength = 10)
	public String henkyakuday;
	@Maxlength(maxlength = 10)
	public String henkyakuyoteiday;
	@Maxlength(maxlength = 10)
	public String hakusu;
	@Maxlength(maxlength = 10)
	public String tanka;
	@Maxlength(maxlength = 10)
	public String waribikino;

	@Maxlength(maxlength = 10)
	public BigInteger minouentaiken;
	@DateType
	@Maxlength(maxlength = 10)
	public String hakkouday;
	
	@Required(target="kariken")
	@Maxlength(maxlength = 10)
	public String karikaiinno;
	
	@Required(target="kai")
	@Maxlength(maxlength = 10)
	public String kaiinno;
	
	@Required(target="karikaiinins,kaiin,kaiinupd,upd,kensaku2")
	@Maxlength(maxlength = 10)
	public String kaiinname;
	
	@Required(target="karikaiinins,kaiin")
	@Maxlength(maxlength = 10)
	public String kaiinhurigana;
	
	@Required(target="karikaiinins,kaiin")
	@Maxlength(maxlength = 10)
	public String gender;
	
	@Required(target="karikaiinins,kaiin")
	@Maxlength(maxlength = 10)
	public String address;
	
	@Required(target="karikaiinins,kaiin")
	@Maxlength(maxlength = 10)
	public String tel;
	
	@Required(target="karikaiinins,kaiin")
	@Maxlength(maxlength = 10)
	public String mail;
	
	@Required(target="karikaiinins,kaiin")
	@Maxlength(maxlength = 10)
	public String birthday;
	
	@Required(target="karikaiinins,kaiin")
	@Maxlength(maxlength = 10)
	public String shokugyo;
	
	@Required(target="karikaiinins,kaiin")
	@Maxlength(maxlength = 10)
	public String mibunshokubun;
	
	@Required(target="karikaiinins,kaiin")
	@Maxlength(maxlength = 10)
	public String mibunshono;
	
	@Required(target="kaiin")
	@Maxlength(maxlength = 10)
	public String taikaiflg;

	@Required(target="kaiin")
	@Maxlength(maxlength = 10)
	public String blackflg;

	@Required(target="kaiin")
	@Maxlength(maxlength = 10)
	public String threeentaicount;

}
