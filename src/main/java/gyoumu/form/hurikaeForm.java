package gyoumu.form;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;
import org.seasar.struts.annotation.DateType;
import org.seasar.struts.annotation.Maxlength;
import org.seasar.struts.annotation.Required;

@Component(instance = InstanceType.SESSION)
public class hurikaeForm implements Serializable {

	private static final long serialVersionUID = 1L;


	
	public String getDvdno() {
		return dvdno;
	}

	public void setDvdno(String dvdno) {
		this.dvdno = dvdno;
	}

	public String getTenpono() {
		return tenpono;
	}

	public void setTenpono(String tenpono) {
		this.tenpono = tenpono;
	}

	@Maxlength(maxlength = 10)
	public String dvdno;

	@Maxlength(maxlength = 10)
	public String tenpono;
	
	public String getKasidasistatus() {
		return kasidasistatus;
	}

	public void setKasidasistatus(String kasidasistatus) {
		this.kasidasistatus = kasidasistatus;
	}

	@Maxlength(maxlength = 10)
	public String kasidasistatus;
}