package gyoumu.form;

import java.io.Serializable;
import java.util.List;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;
import org.seasar.struts.annotation.Required;

@Component(instance = InstanceType.SESSION)
public class KaikeiAddForm implements Serializable {
	private static final long serialVersionUID = 1L;

	public String kaikeikubun;

	public String kaikeinengappi;

	public String getKaikeikubun() {
		return kaikeikubun;
	}

	public void setKaikeikubun(String kaikeikubun) {
		this.kaikeikubun = kaikeikubun;
	}

	public String getKaikeinengappi() {
		return kaikeinengappi;
	}

	public void setKaikeinengappi(String kaikeinengappi) {
		this.kaikeinengappi = kaikeinengappi;
	}

	public String getKingaku() {
		return kingaku;
	}

	public void setKingaku(String kingaku) {
		this.kingaku = kingaku;
	}

	public String getRejino() {
		return rejino;
	}

	public void setRejino(String rejino) {
		this.rejino = rejino;
	}

	public String getTeninno() {
		return teninno;
	}

	public void setTeninno(String teninno) {
		this.teninno = teninno;
	}

	public String getHenkinno() {
		return henkinno;
	}

	public void setHenkinno(String henkinno) {
		this.henkinno = henkinno;
	}

	public String getKaiinno() {
		return kaiinno;
	}

	public void setKaiinno(String kaiinno) {
		this.kaiinno = kaiinno;
	}

	@Required
	public String kingaku;

	public String rejino;

	public String teninno;
	
	@Required
	public String henkinno;

	@Required
	public String kaiinno;

}
