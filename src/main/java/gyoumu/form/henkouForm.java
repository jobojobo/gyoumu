package gyoumu.form;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;
import org.seasar.struts.annotation.DateType;
import org.seasar.struts.annotation.Maxlength;
import org.seasar.struts.annotation.Required;

public class henkouForm implements Serializable{

	
	public String getKaiinname() {
		return kaiinname;
	}

	public void setKaiinname(String kaiinname) {
		this.kaiinname = kaiinname;
	}

	public String getKaiinhurigana() {
		return kaiinhurigana;
	}

	public void setKaiinhurigana(String kaiinhurigana) {
		this.kaiinhurigana = kaiinhurigana;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getShokugyo() {
		return shokugyo;
	}

	public void setShokugyo(String shokugyo) {
		this.shokugyo = shokugyo;
	}

	public String getMibunshokubun() {
		return mibunshokubun;
	}

	public void setMibunshokubun(String mibunshokubun) {
		this.mibunshokubun = mibunshokubun;
	}

	public String getMibunshono() {
		return mibunshono;
	}

	public void setMibunshono(String mibunshono) {
		this.mibunshono = mibunshono;
	}

	@Required
	@Maxlength(maxlength = 10)
	public String kaiinname;
	
	@Required
	@Maxlength(maxlength = 10)
	public String kaiinhurigana;
	
	@Required
	@Maxlength(maxlength = 10)
	public String gender;
	
	@Required
	@Maxlength(maxlength = 20)
	public String address;
	
	@Required
	@Maxlength(maxlength = 10)
	public String tel;
	
	@Required
	@Maxlength(maxlength = 10)
	public String birthday;
	
	@Required
	@Maxlength(maxlength = 10)
	public String mail;
	
	@Required
	@Maxlength(maxlength = 10)
	public String shokugyo;
	
	@Required
	@Maxlength(maxlength = 10)
	public String mibunshokubun;
	
	@Required
	@Maxlength(maxlength = 10)
	public String mibunshono;
}
