package gyoumu.form;

import org.seasar.struts.annotation.Maxlength;
import org.seasar.struts.annotation.Required;

public class LoginForm{
	
	@Required
	@Maxlength(maxlength=6)
	public String teninno;
	
	@Maxlength(maxlength=50)
	public String teninname;
	
	@Maxlength(maxlength=10)
	public String yakushoku;

	@Maxlength(maxlength=10)
	public String rejino;

}
