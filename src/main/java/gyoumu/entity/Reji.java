package gyoumu.entity;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Rejiエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Reji implements Serializable {

    private static final long serialVersionUID = 1L;

    /** rejinoプロパティ */
    @Id
    @Column(length = 6, nullable = false, unique = true)
    public String rejino;

    /** tenponoプロパティ */
    @Column(length = 6, nullable = false, unique = false)
    public String tenpono;

    /** notuseflgプロパティ */
    @Column(length = 1, nullable = true, unique = false)
    public String notuseflg;

    /** kaikeiList関連プロパティ */
    @OneToMany(mappedBy = "reji")
    public List<Kaikei> kaikeiList;

    /** tenpo関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "TENPONO", referencedColumnName = "TENPONO")
    public Tenpo tenpo;

    /** rejisimeList関連プロパティ */
    @OneToMany(mappedBy = "reji")
    public List<Rejisime> rejisimeList;
}