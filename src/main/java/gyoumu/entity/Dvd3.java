package gyoumu.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Dvdエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Dvd3 implements Serializable {

    private static final long serialVersionUID = 1L;

    /** dvdnoプロパティ */
    @Id
    @Column(length = 13, nullable = false, unique = true)
    public String dvdno;

    /** sakuhinnoプロパティ */
    @Column(length = 10, nullable = false, unique = false)
    public String sakuhinno;
    
    /** sakuhinnoプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String title;

    /** kasidasistatusプロパティ */
    @Column(length = 10, nullable = false, unique = false)
    public String kasidasistatus;

    /** nyuukodayプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, unique = false)
    public Date nyuukoday;

    /** baitaiプロパティ */
    @Column(length = 10, nullable = false, unique = false)
    public String baitai;

    /** tenponoプロパティ */
    @Column(length = 6, nullable = false, unique = false)
    public String tenpono;

    /** sakuhin関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "SAKUHINNO", referencedColumnName = "SAKUHINNO")
    public Sakuhin sakuhin;

    /** tenpo関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "TENPONO", referencedColumnName = "TENPONO")
    public Tenpo tenpo;

    /** hurikaeList関連プロパティ */
    @OneToMany(mappedBy = "dvd")
    public List<Hurikae> hurikaeList;

    /** kasidasimeisaiList関連プロパティ */
    @OneToMany(mappedBy = "dvd")
    public List<Kasidasimeisai> kasidasimeisaiList;

    /** toriokiList関連プロパティ */
    @OneToMany(mappedBy = "dvd")
    public List<Torioki> toriokiList;
}