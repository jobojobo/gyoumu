package gyoumu.entity;

import java.io.Serializable;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Toriokiエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Torioki implements Serializable {

    private static final long serialVersionUID = 1L;

    /** sakuhinnoプロパティ */
    @Id
    @Column(length = 10, nullable = false, unique = false)
    public String sakuhinno;

    /** telプロパティ */
    @Id
    @Column(length = 13, nullable = false, unique = false)
    public String tel;

    /** dvdnoプロパティ */
    @Column(length = 13, nullable = true, unique = false)
    public String dvdno;

    /** toriokidayプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, unique = false)
    public Date toriokiday;

    /** dvd関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "DVDNO", referencedColumnName = "DVDNO")
    public Dvd dvd;

    /** sakuhin関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "SAKUHINNO", referencedColumnName = "SAKUHINNO")
    public Sakuhin sakuhin;
}