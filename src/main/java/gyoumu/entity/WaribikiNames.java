package gyoumu.entity;

import gyoumu.entity.KasidasimeisaiNames._KasidasimeisaiNames;
import java.math.BigInteger;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Waribiki}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class WaribikiNames {

    /**
     * waribikinoのプロパティ名を返します。
     * 
     * @return waribikinoのプロパティ名
     */
    public static PropertyName<String> waribikino() {
        return new PropertyName<String>("waribikino");
    }

    /**
     * waribikinameのプロパティ名を返します。
     * 
     * @return waribikinameのプロパティ名
     */
    public static PropertyName<String> waribikiname() {
        return new PropertyName<String>("waribikiname");
    }

    /**
     * waribikigakuのプロパティ名を返します。
     * 
     * @return waribikigakuのプロパティ名
     */
    public static PropertyName<BigInteger> waribikigaku() {
        return new PropertyName<BigInteger>("waribikigaku");
    }

    /**
     * kasidasimeisaiListのプロパティ名を返します。
     * 
     * @return kasidasimeisaiListのプロパティ名
     */
    public static _KasidasimeisaiNames kasidasimeisaiList() {
        return new _KasidasimeisaiNames("kasidasimeisaiList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _WaribikiNames extends PropertyName<Waribiki> {

        /**
         * インスタンスを構築します。
         */
        public _WaribikiNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _WaribikiNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _WaribikiNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * waribikinoのプロパティ名を返します。
         *
         * @return waribikinoのプロパティ名
         */
        public PropertyName<String> waribikino() {
            return new PropertyName<String>(this, "waribikino");
        }

        /**
         * waribikinameのプロパティ名を返します。
         *
         * @return waribikinameのプロパティ名
         */
        public PropertyName<String> waribikiname() {
            return new PropertyName<String>(this, "waribikiname");
        }

        /**
         * waribikigakuのプロパティ名を返します。
         *
         * @return waribikigakuのプロパティ名
         */
        public PropertyName<BigInteger> waribikigaku() {
            return new PropertyName<BigInteger>(this, "waribikigaku");
        }

        /**
         * kasidasimeisaiListのプロパティ名を返します。
         * 
         * @return kasidasimeisaiListのプロパティ名
         */
        public _KasidasimeisaiNames kasidasimeisaiList() {
            return new _KasidasimeisaiNames(this, "kasidasimeisaiList");
        }
    }
}