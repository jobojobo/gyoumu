package gyoumu.entity;

import gyoumu.entity.KaikeiNames._KaikeiNames;
import gyoumu.entity.RejisimeNames._RejisimeNames;
import gyoumu.entity.TenpoNames._TenpoNames;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Reji}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class RejiNames {

    /**
     * rejinoのプロパティ名を返します。
     * 
     * @return rejinoのプロパティ名
     */
    public static PropertyName<String> rejino() {
        return new PropertyName<String>("rejino");
    }

    /**
     * tenponoのプロパティ名を返します。
     * 
     * @return tenponoのプロパティ名
     */
    public static PropertyName<String> tenpono() {
        return new PropertyName<String>("tenpono");
    }

    /**
     * notuseflgのプロパティ名を返します。
     * 
     * @return notuseflgのプロパティ名
     */
    public static PropertyName<String> notuseflg() {
        return new PropertyName<String>("notuseflg");
    }

    /**
     * kaikeiListのプロパティ名を返します。
     * 
     * @return kaikeiListのプロパティ名
     */
    public static _KaikeiNames kaikeiList() {
        return new _KaikeiNames("kaikeiList");
    }

    /**
     * tenpoのプロパティ名を返します。
     * 
     * @return tenpoのプロパティ名
     */
    public static _TenpoNames tenpo() {
        return new _TenpoNames("tenpo");
    }

    /**
     * rejisimeListのプロパティ名を返します。
     * 
     * @return rejisimeListのプロパティ名
     */
    public static _RejisimeNames rejisimeList() {
        return new _RejisimeNames("rejisimeList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _RejiNames extends PropertyName<Reji> {

        /**
         * インスタンスを構築します。
         */
        public _RejiNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _RejiNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _RejiNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * rejinoのプロパティ名を返します。
         *
         * @return rejinoのプロパティ名
         */
        public PropertyName<String> rejino() {
            return new PropertyName<String>(this, "rejino");
        }

        /**
         * tenponoのプロパティ名を返します。
         *
         * @return tenponoのプロパティ名
         */
        public PropertyName<String> tenpono() {
            return new PropertyName<String>(this, "tenpono");
        }

        /**
         * notuseflgのプロパティ名を返します。
         *
         * @return notuseflgのプロパティ名
         */
        public PropertyName<String> notuseflg() {
            return new PropertyName<String>(this, "notuseflg");
        }

        /**
         * kaikeiListのプロパティ名を返します。
         * 
         * @return kaikeiListのプロパティ名
         */
        public _KaikeiNames kaikeiList() {
            return new _KaikeiNames(this, "kaikeiList");
        }

        /**
         * tenpoのプロパティ名を返します。
         * 
         * @return tenpoのプロパティ名
         */
        public _TenpoNames tenpo() {
            return new _TenpoNames(this, "tenpo");
        }

        /**
         * rejisimeListのプロパティ名を返します。
         * 
         * @return rejisimeListのプロパティ名
         */
        public _RejisimeNames rejisimeList() {
            return new _RejisimeNames(this, "rejisimeList");
        }
    }
}