package gyoumu.entity;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Sakuhinkubunエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Sakuhinkubun implements Serializable {

    private static final long serialVersionUID = 1L;

    /** sinsakukubunプロパティ */
    @Id
    @Column(length = 1, nullable = false, unique = true)
    public String sinsakukubun;

    /** sinsakukubunnameプロパティ */
    @Column(length = 50, nullable = false, unique = false)
    public String sinsakukubunname;

    /** ryokinList関連プロパティ */
    @OneToMany(mappedBy = "sakuhinkubun")
    public List<Ryokin> ryokinList;

    /** sakuhinList関連プロパティ */
    @OneToMany(mappedBy = "sakuhinkubun")
    public List<Sakuhin> sakuhinList;
}