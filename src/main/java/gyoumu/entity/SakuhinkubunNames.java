package gyoumu.entity;

import gyoumu.entity.RyokinNames._RyokinNames;
import gyoumu.entity.SakuhinNames._SakuhinNames;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Sakuhinkubun}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class SakuhinkubunNames {

    /**
     * sinsakukubunのプロパティ名を返します。
     * 
     * @return sinsakukubunのプロパティ名
     */
    public static PropertyName<String> sinsakukubun() {
        return new PropertyName<String>("sinsakukubun");
    }

    /**
     * sinsakukubunnameのプロパティ名を返します。
     * 
     * @return sinsakukubunnameのプロパティ名
     */
    public static PropertyName<String> sinsakukubunname() {
        return new PropertyName<String>("sinsakukubunname");
    }

    /**
     * ryokinListのプロパティ名を返します。
     * 
     * @return ryokinListのプロパティ名
     */
    public static _RyokinNames ryokinList() {
        return new _RyokinNames("ryokinList");
    }

    /**
     * sakuhinListのプロパティ名を返します。
     * 
     * @return sakuhinListのプロパティ名
     */
    public static _SakuhinNames sakuhinList() {
        return new _SakuhinNames("sakuhinList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _SakuhinkubunNames extends PropertyName<Sakuhinkubun> {

        /**
         * インスタンスを構築します。
         */
        public _SakuhinkubunNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _SakuhinkubunNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _SakuhinkubunNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * sinsakukubunのプロパティ名を返します。
         *
         * @return sinsakukubunのプロパティ名
         */
        public PropertyName<String> sinsakukubun() {
            return new PropertyName<String>(this, "sinsakukubun");
        }

        /**
         * sinsakukubunnameのプロパティ名を返します。
         *
         * @return sinsakukubunnameのプロパティ名
         */
        public PropertyName<String> sinsakukubunname() {
            return new PropertyName<String>(this, "sinsakukubunname");
        }

        /**
         * ryokinListのプロパティ名を返します。
         * 
         * @return ryokinListのプロパティ名
         */
        public _RyokinNames ryokinList() {
            return new _RyokinNames(this, "ryokinList");
        }

        /**
         * sakuhinListのプロパティ名を返します。
         * 
         * @return sakuhinListのプロパティ名
         */
        public _SakuhinNames sakuhinList() {
            return new _SakuhinNames(this, "sakuhinList");
        }
    }
}