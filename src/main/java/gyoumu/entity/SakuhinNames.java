package gyoumu.entity;

import gyoumu.entity.DvdNames._DvdNames;
import gyoumu.entity.JyanruNames._JyanruNames;
import gyoumu.entity.SakuhinkubunNames._SakuhinkubunNames;
import gyoumu.entity.ToriokiNames._ToriokiNames;
import java.util.Date;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Sakuhin}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class SakuhinNames {

    /**
     * sakuhinnoのプロパティ名を返します。
     * 
     * @return sakuhinnoのプロパティ名
     */
    public static PropertyName<String> sakuhinno() {
        return new PropertyName<String>("sakuhinno");
    }

    /**
     * titleのプロパティ名を返します。
     * 
     * @return titleのプロパティ名
     */
    public static PropertyName<String> title() {
        return new PropertyName<String>("title");
    }

    /**
     * titlekanaのプロパティ名を返します。
     * 
     * @return titlekanaのプロパティ名
     */
    public static PropertyName<String> titlekana() {
        return new PropertyName<String>("titlekana");
    }

    /**
     * shurokutimeのプロパティ名を返します。
     * 
     * @return shurokutimeのプロパティ名
     */
    public static PropertyName<String> shurokutime() {
        return new PropertyName<String>("shurokutime");
    }

    /**
     * rentalstartdayのプロパティ名を返します。
     * 
     * @return rentalstartdayのプロパティ名
     */
    public static PropertyName<Date> rentalstartday() {
        return new PropertyName<Date>("rentalstartday");
    }

    /**
     * jyanrunoのプロパティ名を返します。
     * 
     * @return jyanrunoのプロパティ名
     */
    public static PropertyName<String> jyanruno() {
        return new PropertyName<String>("jyanruno");
    }

    /**
     * sinsakukubunのプロパティ名を返します。
     * 
     * @return sinsakukubunのプロパティ名
     */
    public static PropertyName<String> sinsakukubun() {
        return new PropertyName<String>("sinsakukubun");
    }

    /**
     * shuenのプロパティ名を返します。
     * 
     * @return shuenのプロパティ名
     */
    public static PropertyName<String> shuen() {
        return new PropertyName<String>("shuen");
    }

    /**
     * shuenkanaのプロパティ名を返します。
     * 
     * @return shuenkanaのプロパティ名
     */
    public static PropertyName<String> shuenkana() {
        return new PropertyName<String>("shuenkana");
    }

    /**
     * kantokuのプロパティ名を返します。
     * 
     * @return kantokuのプロパティ名
     */
    public static PropertyName<String> kantoku() {
        return new PropertyName<String>("kantoku");
    }

    /**
     * kantokukanaのプロパティ名を返します。
     * 
     * @return kantokukanaのプロパティ名
     */
    public static PropertyName<String> kantokukana() {
        return new PropertyName<String>("kantokukana");
    }

    /**
     * dvdListのプロパティ名を返します。
     * 
     * @return dvdListのプロパティ名
     */
    public static _DvdNames dvdList() {
        return new _DvdNames("dvdList");
    }

    /**
     * jyanruのプロパティ名を返します。
     * 
     * @return jyanruのプロパティ名
     */
    public static _JyanruNames jyanru() {
        return new _JyanruNames("jyanru");
    }

    /**
     * sakuhinkubunのプロパティ名を返します。
     * 
     * @return sakuhinkubunのプロパティ名
     */
    public static _SakuhinkubunNames sakuhinkubun() {
        return new _SakuhinkubunNames("sakuhinkubun");
    }

    /**
     * toriokiListのプロパティ名を返します。
     * 
     * @return toriokiListのプロパティ名
     */
    public static _ToriokiNames toriokiList() {
        return new _ToriokiNames("toriokiList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _SakuhinNames extends PropertyName<Sakuhin> {

        /**
         * インスタンスを構築します。
         */
        public _SakuhinNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _SakuhinNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _SakuhinNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * sakuhinnoのプロパティ名を返します。
         *
         * @return sakuhinnoのプロパティ名
         */
        public PropertyName<String> sakuhinno() {
            return new PropertyName<String>(this, "sakuhinno");
        }

        /**
         * titleのプロパティ名を返します。
         *
         * @return titleのプロパティ名
         */
        public PropertyName<String> title() {
            return new PropertyName<String>(this, "title");
        }

        /**
         * titlekanaのプロパティ名を返します。
         *
         * @return titlekanaのプロパティ名
         */
        public PropertyName<String> titlekana() {
            return new PropertyName<String>(this, "titlekana");
        }

        /**
         * shurokutimeのプロパティ名を返します。
         *
         * @return shurokutimeのプロパティ名
         */
        public PropertyName<String> shurokutime() {
            return new PropertyName<String>(this, "shurokutime");
        }

        /**
         * rentalstartdayのプロパティ名を返します。
         *
         * @return rentalstartdayのプロパティ名
         */
        public PropertyName<Date> rentalstartday() {
            return new PropertyName<Date>(this, "rentalstartday");
        }

        /**
         * jyanrunoのプロパティ名を返します。
         *
         * @return jyanrunoのプロパティ名
         */
        public PropertyName<String> jyanruno() {
            return new PropertyName<String>(this, "jyanruno");
        }

        /**
         * sinsakukubunのプロパティ名を返します。
         *
         * @return sinsakukubunのプロパティ名
         */
        public PropertyName<String> sinsakukubun() {
            return new PropertyName<String>(this, "sinsakukubun");
        }

        /**
         * shuenのプロパティ名を返します。
         *
         * @return shuenのプロパティ名
         */
        public PropertyName<String> shuen() {
            return new PropertyName<String>(this, "shuen");
        }

        /**
         * shuenkanaのプロパティ名を返します。
         *
         * @return shuenkanaのプロパティ名
         */
        public PropertyName<String> shuenkana() {
            return new PropertyName<String>(this, "shuenkana");
        }

        /**
         * kantokuのプロパティ名を返します。
         *
         * @return kantokuのプロパティ名
         */
        public PropertyName<String> kantoku() {
            return new PropertyName<String>(this, "kantoku");
        }

        /**
         * kantokukanaのプロパティ名を返します。
         *
         * @return kantokukanaのプロパティ名
         */
        public PropertyName<String> kantokukana() {
            return new PropertyName<String>(this, "kantokukana");
        }

        /**
         * dvdListのプロパティ名を返します。
         * 
         * @return dvdListのプロパティ名
         */
        public _DvdNames dvdList() {
            return new _DvdNames(this, "dvdList");
        }

        /**
         * jyanruのプロパティ名を返します。
         * 
         * @return jyanruのプロパティ名
         */
        public _JyanruNames jyanru() {
            return new _JyanruNames(this, "jyanru");
        }

        /**
         * sakuhinkubunのプロパティ名を返します。
         * 
         * @return sakuhinkubunのプロパティ名
         */
        public _SakuhinkubunNames sakuhinkubun() {
            return new _SakuhinkubunNames(this, "sakuhinkubun");
        }

        /**
         * toriokiListのプロパティ名を返します。
         * 
         * @return toriokiListのプロパティ名
         */
        public _ToriokiNames toriokiList() {
            return new _ToriokiNames(this, "toriokiList");
        }
    }
}