package gyoumu.entity;

import gyoumu.entity.DvdNames._DvdNames;
import gyoumu.entity.KaikeiNames._KaikeiNames;
import gyoumu.entity.KikanNames._KikanNames;
import gyoumu.entity.WaribikiNames._WaribikiNames;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Kasidasimeisai}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class KasidasimeisaiNames {

    /**
     * kaikeinoのプロパティ名を返します。
     * 
     * @return kaikeinoのプロパティ名
     */
    public static PropertyName<String> kaikeino() {
        return new PropertyName<String>("kaikeino");
    }

    /**
     * kasidasimeisainoのプロパティ名を返します。
     * 
     * @return kasidasimeisainoのプロパティ名
     */
    public static PropertyName<String> kasidasimeisaino() {
        return new PropertyName<String>("kasidasimeisaino");
    }

    /**
     * dvdnoのプロパティ名を返します。
     * 
     * @return dvdnoのプロパティ名
     */
    public static PropertyName<String> dvdno() {
        return new PropertyName<String>("dvdno");
    }

    /**
     * henkyakudayのプロパティ名を返します。
     * 
     * @return henkyakudayのプロパティ名
     */
    public static PropertyName<Date> henkyakuday() {
        return new PropertyName<Date>("henkyakuday");
    }

    /**
     * henkyakuyoteidayのプロパティ名を返します。
     * 
     * @return henkyakuyoteidayのプロパティ名
     */
    public static PropertyName<Date> henkyakuyoteiday() {
        return new PropertyName<Date>("henkyakuyoteiday");
    }

    /**
     * hakusuのプロパティ名を返します。
     * 
     * @return hakusuのプロパティ名
     */
    public static PropertyName<String> hakusu() {
        return new PropertyName<String>("hakusu");
    }

    /**
     * tankaのプロパティ名を返します。
     * 
     * @return tankaのプロパティ名
     */
    public static PropertyName<BigInteger> tanka() {
        return new PropertyName<BigInteger>("tanka");
    }

    /**
     * waribikinoのプロパティ名を返します。
     * 
     * @return waribikinoのプロパティ名
     */
    public static PropertyName<String> waribikino() {
        return new PropertyName<String>("waribikino");
    }

    /**
     * dvdのプロパティ名を返します。
     * 
     * @return dvdのプロパティ名
     */
    public static _DvdNames dvd() {
        return new _DvdNames("dvd");
    }

    /**
     * kaikeiのプロパティ名を返します。
     * 
     * @return kaikeiのプロパティ名
     */
    public static _KaikeiNames kaikei() {
        return new _KaikeiNames("kaikei");
    }

    /**
     * kikanのプロパティ名を返します。
     * 
     * @return kikanのプロパティ名
     */
    public static _KikanNames kikan() {
        return new _KikanNames("kikan");
    }

    /**
     * waribikiのプロパティ名を返します。
     * 
     * @return waribikiのプロパティ名
     */
    public static _WaribikiNames waribiki() {
        return new _WaribikiNames("waribiki");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _KasidasimeisaiNames extends PropertyName<Kasidasimeisai> {

        /**
         * インスタンスを構築します。
         */
        public _KasidasimeisaiNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _KasidasimeisaiNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _KasidasimeisaiNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * kaikeinoのプロパティ名を返します。
         *
         * @return kaikeinoのプロパティ名
         */
        public PropertyName<String> kaikeino() {
            return new PropertyName<String>(this, "kaikeino");
        }

        /**
         * kasidasimeisainoのプロパティ名を返します。
         *
         * @return kasidasimeisainoのプロパティ名
         */
        public PropertyName<String> kasidasimeisaino() {
            return new PropertyName<String>(this, "kasidasimeisaino");
        }

        /**
         * dvdnoのプロパティ名を返します。
         *
         * @return dvdnoのプロパティ名
         */
        public PropertyName<String> dvdno() {
            return new PropertyName<String>(this, "dvdno");
        }

        /**
         * henkyakudayのプロパティ名を返します。
         *
         * @return henkyakudayのプロパティ名
         */
        public PropertyName<Date> henkyakuday() {
            return new PropertyName<Date>(this, "henkyakuday");
        }

        /**
         * henkyakuyoteidayのプロパティ名を返します。
         *
         * @return henkyakuyoteidayのプロパティ名
         */
        public PropertyName<Date> henkyakuyoteiday() {
            return new PropertyName<Date>(this, "henkyakuyoteiday");
        }

        /**
         * hakusuのプロパティ名を返します。
         *
         * @return hakusuのプロパティ名
         */
        public PropertyName<String> hakusu() {
            return new PropertyName<String>(this, "hakusu");
        }

        /**
         * tankaのプロパティ名を返します。
         *
         * @return tankaのプロパティ名
         */
        public PropertyName<BigInteger> tanka() {
            return new PropertyName<BigInteger>(this, "tanka");
        }

        /**
         * waribikinoのプロパティ名を返します。
         *
         * @return waribikinoのプロパティ名
         */
        public PropertyName<String> waribikino() {
            return new PropertyName<String>(this, "waribikino");
        }

        /**
         * dvdのプロパティ名を返します。
         * 
         * @return dvdのプロパティ名
         */
        public _DvdNames dvd() {
            return new _DvdNames(this, "dvd");
        }

        /**
         * kaikeiのプロパティ名を返します。
         * 
         * @return kaikeiのプロパティ名
         */
        public _KaikeiNames kaikei() {
            return new _KaikeiNames(this, "kaikei");
        }

        /**
         * kikanのプロパティ名を返します。
         * 
         * @return kikanのプロパティ名
         */
        public _KikanNames kikan() {
            return new _KikanNames(this, "kikan");
        }

        /**
         * waribikiのプロパティ名を返します。
         * 
         * @return waribikiのプロパティ名
         */
        public _WaribikiNames waribiki() {
            return new _WaribikiNames(this, "waribiki");
        }
    }
}