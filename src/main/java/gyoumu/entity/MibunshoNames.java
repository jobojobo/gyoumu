package gyoumu.entity;

import gyoumu.entity.KaiinNames._KaiinNames;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Mibunsho}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class MibunshoNames {

    /**
     * mibunshokubunのプロパティ名を返します。
     * 
     * @return mibunshokubunのプロパティ名
     */
    public static PropertyName<String> mibunshokubun() {
        return new PropertyName<String>("mibunshokubun");
    }

    /**
     * mibunshonameのプロパティ名を返します。
     * 
     * @return mibunshonameのプロパティ名
     */
    public static PropertyName<String> mibunshoname() {
        return new PropertyName<String>("mibunshoname");
    }

    /**
     * kaiinListのプロパティ名を返します。
     * 
     * @return kaiinListのプロパティ名
     */
    public static _KaiinNames kaiinList() {
        return new _KaiinNames("kaiinList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _MibunshoNames extends PropertyName<Mibunsho> {

        /**
         * インスタンスを構築します。
         */
        public _MibunshoNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _MibunshoNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _MibunshoNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * mibunshokubunのプロパティ名を返します。
         *
         * @return mibunshokubunのプロパティ名
         */
        public PropertyName<String> mibunshokubun() {
            return new PropertyName<String>(this, "mibunshokubun");
        }

        /**
         * mibunshonameのプロパティ名を返します。
         *
         * @return mibunshonameのプロパティ名
         */
        public PropertyName<String> mibunshoname() {
            return new PropertyName<String>(this, "mibunshoname");
        }

        /**
         * kaiinListのプロパティ名を返します。
         * 
         * @return kaiinListのプロパティ名
         */
        public _KaiinNames kaiinList() {
            return new _KaiinNames(this, "kaiinList");
        }
    }
}