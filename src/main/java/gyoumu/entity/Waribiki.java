package gyoumu.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Waribikiエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Waribiki implements Serializable {

    private static final long serialVersionUID = 1L;

    /** waribikinoプロパティ */
    @Id
    @Column(length = 6, nullable = false, unique = true)
    public String waribikino;

    /** waribikinameプロパティ */
    @Column(length = 50, nullable = false, unique = false)
    public String waribikiname;

    /** waribikigakuプロパティ */
    @Column(precision = 38, nullable = false, unique = false)
    public BigInteger waribikigaku;

    /** kasidasimeisaiList関連プロパティ */
    @OneToMany(mappedBy = "waribiki")
    public List<Kasidasimeisai> kasidasimeisaiList;
}