package gyoumu.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Kaiinエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Kaiin4 implements Serializable {

    private static final long serialVersionUID = 1L;

    /** kaiinnoプロパティ */
    @Id
    @Column(length = 6, nullable = false, unique = true)
    public String kaiinno;

    /** kaiinnameプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String kaiinname;

    /** kaiinnameプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String title;
    
    /** birthdayプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, unique = false)
    public Date henkyakuyoteiday;
    
    /** kaiinhuriganaプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String tenponame;
    
    /** kaiinhuriganaプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String kaiinhurigana;

    /** genderプロパティ */
    @Column(length = 2, nullable = false, unique = false)
    public String gender;

    /** addressプロパティ */
    @Column(length = 160, nullable = false, unique = false)
    public String address;

    /** telプロパティ */
    @Column(length = 13, nullable = false, unique = false)
    public String tel;

    /** mailプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String mail;

    /** birthdayプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, unique = false)
    public Date birthday;

    /** shokugyoプロパティ */
    @Column(length = 16, nullable = false, unique = false)
    public String shokugyo;

    /** mibunshokubunプロパティ */
    @Column(length = 2, nullable = false, unique = false)
    public String mibunshokubun;

    /** mibunshonoプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String mibunshono;

    /** taikaiflgプロパティ */
    @Column(length = 1, nullable = false, unique = false)
    public String taikaiflg;

    /** hakkoudayプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, unique = false)
    public Date hakkouday;
    


    /** blackflgプロパティ */
    @Column(length = 1, nullable = false, unique = false)
    public String blackflg;

    /** threeentaicountプロパティ */
    @Column(precision = 38, nullable = false, unique = false)
    public BigInteger threeentaicount;

    /** minouentaikenプロパティ */
    @Column(precision = 38, nullable = false, unique = false)
    public BigInteger minouentaiken;

    /** mibunsho関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "MIBUNSHOKUBUN", referencedColumnName = "MIBUNSHOKUBUN")
    public Mibunsho mibunsho;

    /** kaikeiList関連プロパティ */
    @OneToMany(mappedBy = "kaiin")
    public List<Kaikei> kaikeiList;
}