package gyoumu.entity;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Tenpoエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Tenpo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** tenponoプロパティ */
    @Id
    @Column(length = 6, nullable = false, unique = true)
    public String tenpono;

    /** tenponameプロパティ */
    @Column(length = 50, nullable = false, unique = false)
    public String tenponame;

    /** tenpoadressプロパティ */
    @Column(length = 160, nullable = false, unique = false)
    public String tenpoadress;

    /** heitenflgプロパティ */
    @Column(length = 1, nullable = true, unique = false)
    public String heitenflg;

    /** dvdList関連プロパティ */
    @OneToMany(mappedBy = "tenpo")
    public List<Dvd> dvdList;

    /** hurikaeList関連プロパティ */
    @OneToMany(mappedBy = "tenpo")
    public List<Hurikae> hurikaeList;

    /** rejiList関連プロパティ */
    @OneToMany(mappedBy = "tenpo")
    public List<Reji> rejiList;
}