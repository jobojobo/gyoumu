package gyoumu.entity;

import gyoumu.entity.KasidasimeisaiNames._KasidasimeisaiNames;
import gyoumu.entity.RyokinNames._RyokinNames;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Kikan}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class KikanNames {

    /**
     * hakusuのプロパティ名を返します。
     * 
     * @return hakusuのプロパティ名
     */
    public static PropertyName<String> hakusu() {
        return new PropertyName<String>("hakusu");
    }

    /**
     * hakusunameのプロパティ名を返します。
     * 
     * @return hakusunameのプロパティ名
     */
    public static PropertyName<String> hakusuname() {
        return new PropertyName<String>("hakusuname");
    }

    /**
     * kasidasimeisaiListのプロパティ名を返します。
     * 
     * @return kasidasimeisaiListのプロパティ名
     */
    public static _KasidasimeisaiNames kasidasimeisaiList() {
        return new _KasidasimeisaiNames("kasidasimeisaiList");
    }

    /**
     * ryokinListのプロパティ名を返します。
     * 
     * @return ryokinListのプロパティ名
     */
    public static _RyokinNames ryokinList() {
        return new _RyokinNames("ryokinList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _KikanNames extends PropertyName<Kikan> {

        /**
         * インスタンスを構築します。
         */
        public _KikanNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _KikanNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _KikanNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * hakusuのプロパティ名を返します。
         *
         * @return hakusuのプロパティ名
         */
        public PropertyName<String> hakusu() {
            return new PropertyName<String>(this, "hakusu");
        }

        /**
         * hakusunameのプロパティ名を返します。
         *
         * @return hakusunameのプロパティ名
         */
        public PropertyName<String> hakusuname() {
            return new PropertyName<String>(this, "hakusuname");
        }

        /**
         * kasidasimeisaiListのプロパティ名を返します。
         * 
         * @return kasidasimeisaiListのプロパティ名
         */
        public _KasidasimeisaiNames kasidasimeisaiList() {
            return new _KasidasimeisaiNames(this, "kasidasimeisaiList");
        }

        /**
         * ryokinListのプロパティ名を返します。
         * 
         * @return ryokinListのプロパティ名
         */
        public _RyokinNames ryokinList() {
            return new _RyokinNames(this, "ryokinList");
        }
    }
}