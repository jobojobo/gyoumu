package gyoumu.entity;

import gyoumu.entity.DvdNames._DvdNames;
import gyoumu.entity.SakuhinNames._SakuhinNames;
import java.util.Date;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Torioki}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class ToriokiNames {

    /**
     * sakuhinnoのプロパティ名を返します。
     * 
     * @return sakuhinnoのプロパティ名
     */
    public static PropertyName<String> sakuhinno() {
        return new PropertyName<String>("sakuhinno");
    }

    /**
     * telのプロパティ名を返します。
     * 
     * @return telのプロパティ名
     */
    public static PropertyName<String> tel() {
        return new PropertyName<String>("tel");
    }

    /**
     * dvdnoのプロパティ名を返します。
     * 
     * @return dvdnoのプロパティ名
     */
    public static PropertyName<String> dvdno() {
        return new PropertyName<String>("dvdno");
    }

    /**
     * toriokidayのプロパティ名を返します。
     * 
     * @return toriokidayのプロパティ名
     */
    public static PropertyName<Date> toriokiday() {
        return new PropertyName<Date>("toriokiday");
    }

    /**
     * dvdのプロパティ名を返します。
     * 
     * @return dvdのプロパティ名
     */
    public static _DvdNames dvd() {
        return new _DvdNames("dvd");
    }

    /**
     * sakuhinのプロパティ名を返します。
     * 
     * @return sakuhinのプロパティ名
     */
    public static _SakuhinNames sakuhin() {
        return new _SakuhinNames("sakuhin");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _ToriokiNames extends PropertyName<Torioki> {

        /**
         * インスタンスを構築します。
         */
        public _ToriokiNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _ToriokiNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _ToriokiNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * sakuhinnoのプロパティ名を返します。
         *
         * @return sakuhinnoのプロパティ名
         */
        public PropertyName<String> sakuhinno() {
            return new PropertyName<String>(this, "sakuhinno");
        }

        /**
         * telのプロパティ名を返します。
         *
         * @return telのプロパティ名
         */
        public PropertyName<String> tel() {
            return new PropertyName<String>(this, "tel");
        }

        /**
         * dvdnoのプロパティ名を返します。
         *
         * @return dvdnoのプロパティ名
         */
        public PropertyName<String> dvdno() {
            return new PropertyName<String>(this, "dvdno");
        }

        /**
         * toriokidayのプロパティ名を返します。
         *
         * @return toriokidayのプロパティ名
         */
        public PropertyName<Date> toriokiday() {
            return new PropertyName<Date>(this, "toriokiday");
        }

        /**
         * dvdのプロパティ名を返します。
         * 
         * @return dvdのプロパティ名
         */
        public _DvdNames dvd() {
            return new _DvdNames(this, "dvd");
        }

        /**
         * sakuhinのプロパティ名を返します。
         * 
         * @return sakuhinのプロパティ名
         */
        public _SakuhinNames sakuhin() {
            return new _SakuhinNames(this, "sakuhin");
        }
    }
}