package gyoumu.entity;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Jyanruエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Jyanru implements Serializable {

    private static final long serialVersionUID = 1L;

    /** jyanrunoプロパティ */
    @Id
    @Column(length = 2, nullable = false, unique = true)
    public String jyanruno;

    /** jyanrunameプロパティ */
    @Column(length = 20, nullable = false, unique = false)
    public String jyanruname;

    /** sakuhinList関連プロパティ */
    @OneToMany(mappedBy = "jyanru")
    public List<Sakuhin> sakuhinList;
}