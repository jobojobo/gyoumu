package gyoumu.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Sakuhinエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Sakuhin implements Serializable {

    private static final long serialVersionUID = 1L;

    /** sakuhinnoプロパティ */
    @Id
    @Column(length = 10, nullable = false, unique = true)
    public String sakuhinno;

    /** titleプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String title;

    /** titlekanaプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String titlekana;

    /** shurokutimeプロパティ */
    @Column(length = 3, nullable = false, unique = false)
    public String shurokutime;

    /** rentalstartdayプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, unique = false)
    public Date rentalstartday;

    /** jyanrunoプロパティ */
    @Column(length = 2, nullable = false, unique = false)
    public String jyanruno;

    /** sinsakukubunプロパティ */
    @Column(length = 1, nullable = false, unique = false)
    public String sinsakukubun;

    /** shuenプロパティ */
    @Column(length = 100, nullable = true, unique = false)
    public String shuen;

    /** shuenkanaプロパティ */
    @Column(length = 100, nullable = true, unique = false)
    public String shuenkana;

    /** kantokuプロパティ */
    @Column(length = 100, nullable = true, unique = false)
    public String kantoku;

    /** kantokukanaプロパティ */
    @Column(length = 100, nullable = true, unique = false)
    public String kantokukana;

    /** dvdList関連プロパティ */
    @OneToMany(mappedBy = "sakuhin")
    public List<Dvd> dvdList;

    /** jyanru関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "JYANRUNO", referencedColumnName = "JYANRUNO")
    public Jyanru jyanru;

    /** sakuhinkubun関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "SINSAKUKUBUN", referencedColumnName = "SINSAKUKUBUN")
    public Sakuhinkubun sakuhinkubun;

    /** toriokiList関連プロパティ */
    @OneToMany(mappedBy = "sakuhin")
    public List<Torioki> toriokiList;
}