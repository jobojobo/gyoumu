package gyoumu.entity;

import java.io.Serializable;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Hurikaeエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Hurikae implements Serializable {

    private static final long serialVersionUID = 1L;

    /** tenponoプロパティ */
    @Id
    @Column(length = 6, nullable = false, unique = false)
    public String tenpono;

    /** sinseidayプロパティ */
    @Id
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, unique = false)
    public Date sinseiday;

    /** dvdnoプロパティ */
    @Id
    @Column(length = 13, nullable = false, unique = false)
    public String dvdno;

    /** shoninflgプロパティ */
    @Column(length = 1, nullable = false, unique = false)
    public String shoninflg;

    /** kashidasidayプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true, unique = false)
    public Date kashidasiday;

    /** henkyakuyoteidayプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true, unique = false)
    public Date henkyakuyoteiday;

    /** henkyakudayプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true, unique = false)
    public Date henkyakuday;

    /** dvd関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "DVDNO", referencedColumnName = "DVDNO")
    public Dvd dvd;

    /** tenpo関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "TENPONO", referencedColumnName = "TENPONO")
    public Tenpo tenpo;
}