package gyoumu.entity;

import gyoumu.entity.KikanNames._KikanNames;
import gyoumu.entity.SakuhinkubunNames._SakuhinkubunNames;
import java.math.BigInteger;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Ryokin}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class RyokinNames {

    /**
     * hakusuのプロパティ名を返します。
     * 
     * @return hakusuのプロパティ名
     */
    public static PropertyName<String> hakusu() {
        return new PropertyName<String>("hakusu");
    }

    /**
     * sinsakukubunのプロパティ名を返します。
     * 
     * @return sinsakukubunのプロパティ名
     */
    public static PropertyName<String> sinsakukubun() {
        return new PropertyName<String>("sinsakukubun");
    }

    /**
     * ryokinのプロパティ名を返します。
     * 
     * @return ryokinのプロパティ名
     */
    public static PropertyName<BigInteger> ryokin() {
        return new PropertyName<BigInteger>("ryokin");
    }

    /**
     * kikanのプロパティ名を返します。
     * 
     * @return kikanのプロパティ名
     */
    public static _KikanNames kikan() {
        return new _KikanNames("kikan");
    }

    /**
     * sakuhinkubunのプロパティ名を返します。
     * 
     * @return sakuhinkubunのプロパティ名
     */
    public static _SakuhinkubunNames sakuhinkubun() {
        return new _SakuhinkubunNames("sakuhinkubun");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _RyokinNames extends PropertyName<Ryokin> {

        /**
         * インスタンスを構築します。
         */
        public _RyokinNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _RyokinNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _RyokinNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * hakusuのプロパティ名を返します。
         *
         * @return hakusuのプロパティ名
         */
        public PropertyName<String> hakusu() {
            return new PropertyName<String>(this, "hakusu");
        }

        /**
         * sinsakukubunのプロパティ名を返します。
         *
         * @return sinsakukubunのプロパティ名
         */
        public PropertyName<String> sinsakukubun() {
            return new PropertyName<String>(this, "sinsakukubun");
        }

        /**
         * ryokinのプロパティ名を返します。
         *
         * @return ryokinのプロパティ名
         */
        public PropertyName<BigInteger> ryokin() {
            return new PropertyName<BigInteger>(this, "ryokin");
        }

        /**
         * kikanのプロパティ名を返します。
         * 
         * @return kikanのプロパティ名
         */
        public _KikanNames kikan() {
            return new _KikanNames(this, "kikan");
        }

        /**
         * sakuhinkubunのプロパティ名を返します。
         * 
         * @return sakuhinkubunのプロパティ名
         */
        public _SakuhinkubunNames sakuhinkubun() {
            return new _SakuhinkubunNames(this, "sakuhinkubun");
        }
    }
}