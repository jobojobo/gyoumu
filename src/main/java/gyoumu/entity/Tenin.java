package gyoumu.entity;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Teninエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Tenin implements Serializable {

    private static final long serialVersionUID = 1L;

    /** teninnoプロパティ */
    @Id
    @Column(length = 6, nullable = false, unique = true)
    public String teninno;

    /** teninnameプロパティ */
    @Column(length = 50, nullable = false, unique = false)
    public String teninname;

    /** yakushokuプロパティ */
    @Column(length = 10, nullable = false, unique = false)
    public String yakushoku;

    /** taisyaflgプロパティ */
    @Column(length = 1, nullable = false, unique = false)
    public String taisyaflg;

    /** kaikeiList関連プロパティ */
    @OneToMany(mappedBy = "tenin")
    public List<Kaikei> kaikeiList;

    /** rejisimeList関連プロパティ */
    @OneToMany(mappedBy = "tenin")
    public List<Rejisime> rejisimeList;
}