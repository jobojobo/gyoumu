package gyoumu.entity;

import gyoumu.entity.DvdNames._DvdNames;
import gyoumu.entity.HurikaeNames._HurikaeNames;
import gyoumu.entity.JyanruNames._JyanruNames;
import gyoumu.entity.KaiinNames._KaiinNames;
import gyoumu.entity.KaikeiNames._KaikeiNames;
import gyoumu.entity.KarikaiinNames._KarikaiinNames;
import gyoumu.entity.KasidasimeisaiNames._KasidasimeisaiNames;
import gyoumu.entity.KikanNames._KikanNames;
import gyoumu.entity.MibunshoNames._MibunshoNames;
import gyoumu.entity.RejiNames._RejiNames;
import gyoumu.entity.RejisimeNames._RejisimeNames;
import gyoumu.entity.RyokinNames._RyokinNames;
import gyoumu.entity.SakuhinNames._SakuhinNames;
import gyoumu.entity.SakuhinkubunNames._SakuhinkubunNames;
import gyoumu.entity.ShiharaikubunNames._ShiharaikubunNames;
import gyoumu.entity.TeninNames._TeninNames;
import gyoumu.entity.TenpoNames._TenpoNames;
import gyoumu.entity.ToriokiNames._ToriokiNames;
import gyoumu.entity.WaribikiNames._WaribikiNames;
import javax.annotation.Generated;

/**
 * 名前クラスの集約です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesAggregateModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class Names {

    /**
     * {@link Dvd}の名前クラスを返します。
     * 
     * @return Dvdの名前クラス
     */
    public static _DvdNames dvd() {
        return new _DvdNames();
    }

    /**
     * {@link Hurikae}の名前クラスを返します。
     * 
     * @return Hurikaeの名前クラス
     */
    public static _HurikaeNames hurikae() {
        return new _HurikaeNames();
    }

    /**
     * {@link Jyanru}の名前クラスを返します。
     * 
     * @return Jyanruの名前クラス
     */
    public static _JyanruNames jyanru() {
        return new _JyanruNames();
    }

    /**
     * {@link Kaiin}の名前クラスを返します。
     * 
     * @return Kaiinの名前クラス
     */
    public static _KaiinNames kaiin() {
        return new _KaiinNames();
    }

    /**
     * {@link Kaikei}の名前クラスを返します。
     * 
     * @return Kaikeiの名前クラス
     */
    public static _KaikeiNames kaikei() {
        return new _KaikeiNames();
    }

    /**
     * {@link Karikaiin}の名前クラスを返します。
     * 
     * @return Karikaiinの名前クラス
     */
    public static _KarikaiinNames karikaiin() {
        return new _KarikaiinNames();
    }

    /**
     * {@link Kasidasimeisai}の名前クラスを返します。
     * 
     * @return Kasidasimeisaiの名前クラス
     */
    public static _KasidasimeisaiNames kasidasimeisai() {
        return new _KasidasimeisaiNames();
    }

    /**
     * {@link Kikan}の名前クラスを返します。
     * 
     * @return Kikanの名前クラス
     */
    public static _KikanNames kikan() {
        return new _KikanNames();
    }

    /**
     * {@link Mibunsho}の名前クラスを返します。
     * 
     * @return Mibunshoの名前クラス
     */
    public static _MibunshoNames mibunsho() {
        return new _MibunshoNames();
    }

    /**
     * {@link Reji}の名前クラスを返します。
     * 
     * @return Rejiの名前クラス
     */
    public static _RejiNames reji() {
        return new _RejiNames();
    }

    /**
     * {@link Rejisime}の名前クラスを返します。
     * 
     * @return Rejisimeの名前クラス
     */
    public static _RejisimeNames rejisime() {
        return new _RejisimeNames();
    }

    /**
     * {@link Ryokin}の名前クラスを返します。
     * 
     * @return Ryokinの名前クラス
     */
    public static _RyokinNames ryokin() {
        return new _RyokinNames();
    }

    /**
     * {@link Sakuhin}の名前クラスを返します。
     * 
     * @return Sakuhinの名前クラス
     */
    public static _SakuhinNames sakuhin() {
        return new _SakuhinNames();
    }

    /**
     * {@link Sakuhinkubun}の名前クラスを返します。
     * 
     * @return Sakuhinkubunの名前クラス
     */
    public static _SakuhinkubunNames sakuhinkubun() {
        return new _SakuhinkubunNames();
    }

    /**
     * {@link Shiharaikubun}の名前クラスを返します。
     * 
     * @return Shiharaikubunの名前クラス
     */
    public static _ShiharaikubunNames shiharaikubun() {
        return new _ShiharaikubunNames();
    }

    /**
     * {@link Tenin}の名前クラスを返します。
     * 
     * @return Teninの名前クラス
     */
    public static _TeninNames tenin() {
        return new _TeninNames();
    }

    /**
     * {@link Tenpo}の名前クラスを返します。
     * 
     * @return Tenpoの名前クラス
     */
    public static _TenpoNames tenpo() {
        return new _TenpoNames();
    }

    /**
     * {@link Torioki}の名前クラスを返します。
     * 
     * @return Toriokiの名前クラス
     */
    public static _ToriokiNames torioki() {
        return new _ToriokiNames();
    }

    /**
     * {@link Waribiki}の名前クラスを返します。
     * 
     * @return Waribikiの名前クラス
     */
    public static _WaribikiNames waribiki() {
        return new _WaribikiNames();
    }
}