package gyoumu.entity;

import gyoumu.entity.KaikeiNames._KaikeiNames;
import gyoumu.entity.MibunshoNames._MibunshoNames;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Kaiin}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class KaiinNames {

    /**
     * kaiinnoのプロパティ名を返します。
     * 
     * @return kaiinnoのプロパティ名
     */
    public static PropertyName<String> kaiinno() {
        return new PropertyName<String>("kaiinno");
    }

    /**
     * kaiinnameのプロパティ名を返します。
     * 
     * @return kaiinnameのプロパティ名
     */
    public static PropertyName<String> kaiinname() {
        return new PropertyName<String>("kaiinname");
    }

    /**
     * kaiinhuriganaのプロパティ名を返します。
     * 
     * @return kaiinhuriganaのプロパティ名
     */
    public static PropertyName<String> kaiinhurigana() {
        return new PropertyName<String>("kaiinhurigana");
    }

    /**
     * genderのプロパティ名を返します。
     * 
     * @return genderのプロパティ名
     */
    public static PropertyName<String> gender() {
        return new PropertyName<String>("gender");
    }

    /**
     * addressのプロパティ名を返します。
     * 
     * @return addressのプロパティ名
     */
    public static PropertyName<String> address() {
        return new PropertyName<String>("address");
    }

    /**
     * telのプロパティ名を返します。
     * 
     * @return telのプロパティ名
     */
    public static PropertyName<String> tel() {
        return new PropertyName<String>("tel");
    }

    /**
     * mailのプロパティ名を返します。
     * 
     * @return mailのプロパティ名
     */
    public static PropertyName<String> mail() {
        return new PropertyName<String>("mail");
    }

    /**
     * birthdayのプロパティ名を返します。
     * 
     * @return birthdayのプロパティ名
     */
    public static PropertyName<Date> birthday() {
        return new PropertyName<Date>("birthday");
    }

    /**
     * shokugyoのプロパティ名を返します。
     * 
     * @return shokugyoのプロパティ名
     */
    public static PropertyName<String> shokugyo() {
        return new PropertyName<String>("shokugyo");
    }

    /**
     * mibunshokubunのプロパティ名を返します。
     * 
     * @return mibunshokubunのプロパティ名
     */
    public static PropertyName<String> mibunshokubun() {
        return new PropertyName<String>("mibunshokubun");
    }

    /**
     * mibunshonoのプロパティ名を返します。
     * 
     * @return mibunshonoのプロパティ名
     */
    public static PropertyName<String> mibunshono() {
        return new PropertyName<String>("mibunshono");
    }

    /**
     * taikaiflgのプロパティ名を返します。
     * 
     * @return taikaiflgのプロパティ名
     */
    public static PropertyName<String> taikaiflg() {
        return new PropertyName<String>("taikaiflg");
    }

    /**
     * hakkoudayのプロパティ名を返します。
     * 
     * @return hakkoudayのプロパティ名
     */
    public static PropertyName<Date> hakkouday() {
        return new PropertyName<Date>("hakkouday");
    }

    /**
     * blackflgのプロパティ名を返します。
     * 
     * @return blackflgのプロパティ名
     */
    public static PropertyName<String> blackflg() {
        return new PropertyName<String>("blackflg");
    }

    /**
     * threeentaicountのプロパティ名を返します。
     * 
     * @return threeentaicountのプロパティ名
     */
    public static PropertyName<BigInteger> threeentaicount() {
        return new PropertyName<BigInteger>("threeentaicount");
    }

    /**
     * minouentaikenのプロパティ名を返します。
     * 
     * @return minouentaikenのプロパティ名
     */
    public static PropertyName<BigInteger> minouentaiken() {
        return new PropertyName<BigInteger>("minouentaiken");
    }

    /**
     * mibunshoのプロパティ名を返します。
     * 
     * @return mibunshoのプロパティ名
     */
    public static _MibunshoNames mibunsho() {
        return new _MibunshoNames("mibunsho");
    }

    /**
     * kaikeiListのプロパティ名を返します。
     * 
     * @return kaikeiListのプロパティ名
     */
    public static _KaikeiNames kaikeiList() {
        return new _KaikeiNames("kaikeiList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _KaiinNames extends PropertyName<Kaiin> {

        /**
         * インスタンスを構築します。
         */
        public _KaiinNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _KaiinNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _KaiinNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * kaiinnoのプロパティ名を返します。
         *
         * @return kaiinnoのプロパティ名
         */
        public PropertyName<String> kaiinno() {
            return new PropertyName<String>(this, "kaiinno");
        }

        /**
         * kaiinnameのプロパティ名を返します。
         *
         * @return kaiinnameのプロパティ名
         */
        public PropertyName<String> kaiinname() {
            return new PropertyName<String>(this, "kaiinname");
        }

        /**
         * kaiinhuriganaのプロパティ名を返します。
         *
         * @return kaiinhuriganaのプロパティ名
         */
        public PropertyName<String> kaiinhurigana() {
            return new PropertyName<String>(this, "kaiinhurigana");
        }

        /**
         * genderのプロパティ名を返します。
         *
         * @return genderのプロパティ名
         */
        public PropertyName<String> gender() {
            return new PropertyName<String>(this, "gender");
        }

        /**
         * addressのプロパティ名を返します。
         *
         * @return addressのプロパティ名
         */
        public PropertyName<String> address() {
            return new PropertyName<String>(this, "address");
        }

        /**
         * telのプロパティ名を返します。
         *
         * @return telのプロパティ名
         */
        public PropertyName<String> tel() {
            return new PropertyName<String>(this, "tel");
        }

        /**
         * mailのプロパティ名を返します。
         *
         * @return mailのプロパティ名
         */
        public PropertyName<String> mail() {
            return new PropertyName<String>(this, "mail");
        }

        /**
         * birthdayのプロパティ名を返します。
         *
         * @return birthdayのプロパティ名
         */
        public PropertyName<Date> birthday() {
            return new PropertyName<Date>(this, "birthday");
        }

        /**
         * shokugyoのプロパティ名を返します。
         *
         * @return shokugyoのプロパティ名
         */
        public PropertyName<String> shokugyo() {
            return new PropertyName<String>(this, "shokugyo");
        }

        /**
         * mibunshokubunのプロパティ名を返します。
         *
         * @return mibunshokubunのプロパティ名
         */
        public PropertyName<String> mibunshokubun() {
            return new PropertyName<String>(this, "mibunshokubun");
        }

        /**
         * mibunshonoのプロパティ名を返します。
         *
         * @return mibunshonoのプロパティ名
         */
        public PropertyName<String> mibunshono() {
            return new PropertyName<String>(this, "mibunshono");
        }

        /**
         * taikaiflgのプロパティ名を返します。
         *
         * @return taikaiflgのプロパティ名
         */
        public PropertyName<String> taikaiflg() {
            return new PropertyName<String>(this, "taikaiflg");
        }

        /**
         * hakkoudayのプロパティ名を返します。
         *
         * @return hakkoudayのプロパティ名
         */
        public PropertyName<Date> hakkouday() {
            return new PropertyName<Date>(this, "hakkouday");
        }

        /**
         * blackflgのプロパティ名を返します。
         *
         * @return blackflgのプロパティ名
         */
        public PropertyName<String> blackflg() {
            return new PropertyName<String>(this, "blackflg");
        }

        /**
         * threeentaicountのプロパティ名を返します。
         *
         * @return threeentaicountのプロパティ名
         */
        public PropertyName<BigInteger> threeentaicount() {
            return new PropertyName<BigInteger>(this, "threeentaicount");
        }

        /**
         * minouentaikenのプロパティ名を返します。
         *
         * @return minouentaikenのプロパティ名
         */
        public PropertyName<BigInteger> minouentaiken() {
            return new PropertyName<BigInteger>(this, "minouentaiken");
        }

        /**
         * mibunshoのプロパティ名を返します。
         * 
         * @return mibunshoのプロパティ名
         */
        public _MibunshoNames mibunsho() {
            return new _MibunshoNames(this, "mibunsho");
        }

        /**
         * kaikeiListのプロパティ名を返します。
         * 
         * @return kaikeiListのプロパティ名
         */
        public _KaikeiNames kaikeiList() {
            return new _KaikeiNames(this, "kaikeiList");
        }
    }
}