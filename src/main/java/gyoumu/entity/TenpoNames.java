package gyoumu.entity;

import gyoumu.entity.DvdNames._DvdNames;
import gyoumu.entity.HurikaeNames._HurikaeNames;
import gyoumu.entity.RejiNames._RejiNames;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Tenpo}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class TenpoNames {

    /**
     * tenponoのプロパティ名を返します。
     * 
     * @return tenponoのプロパティ名
     */
    public static PropertyName<String> tenpono() {
        return new PropertyName<String>("tenpono");
    }

    /**
     * tenponameのプロパティ名を返します。
     * 
     * @return tenponameのプロパティ名
     */
    public static PropertyName<String> tenponame() {
        return new PropertyName<String>("tenponame");
    }

    /**
     * tenpoadressのプロパティ名を返します。
     * 
     * @return tenpoadressのプロパティ名
     */
    public static PropertyName<String> tenpoadress() {
        return new PropertyName<String>("tenpoadress");
    }

    /**
     * heitenflgのプロパティ名を返します。
     * 
     * @return heitenflgのプロパティ名
     */
    public static PropertyName<String> heitenflg() {
        return new PropertyName<String>("heitenflg");
    }

    /**
     * dvdListのプロパティ名を返します。
     * 
     * @return dvdListのプロパティ名
     */
    public static _DvdNames dvdList() {
        return new _DvdNames("dvdList");
    }

    /**
     * hurikaeListのプロパティ名を返します。
     * 
     * @return hurikaeListのプロパティ名
     */
    public static _HurikaeNames hurikaeList() {
        return new _HurikaeNames("hurikaeList");
    }

    /**
     * rejiListのプロパティ名を返します。
     * 
     * @return rejiListのプロパティ名
     */
    public static _RejiNames rejiList() {
        return new _RejiNames("rejiList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _TenpoNames extends PropertyName<Tenpo> {

        /**
         * インスタンスを構築します。
         */
        public _TenpoNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _TenpoNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _TenpoNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * tenponoのプロパティ名を返します。
         *
         * @return tenponoのプロパティ名
         */
        public PropertyName<String> tenpono() {
            return new PropertyName<String>(this, "tenpono");
        }

        /**
         * tenponameのプロパティ名を返します。
         *
         * @return tenponameのプロパティ名
         */
        public PropertyName<String> tenponame() {
            return new PropertyName<String>(this, "tenponame");
        }

        /**
         * tenpoadressのプロパティ名を返します。
         *
         * @return tenpoadressのプロパティ名
         */
        public PropertyName<String> tenpoadress() {
            return new PropertyName<String>(this, "tenpoadress");
        }

        /**
         * heitenflgのプロパティ名を返します。
         *
         * @return heitenflgのプロパティ名
         */
        public PropertyName<String> heitenflg() {
            return new PropertyName<String>(this, "heitenflg");
        }

        /**
         * dvdListのプロパティ名を返します。
         * 
         * @return dvdListのプロパティ名
         */
        public _DvdNames dvdList() {
            return new _DvdNames(this, "dvdList");
        }

        /**
         * hurikaeListのプロパティ名を返します。
         * 
         * @return hurikaeListのプロパティ名
         */
        public _HurikaeNames hurikaeList() {
            return new _HurikaeNames(this, "hurikaeList");
        }

        /**
         * rejiListのプロパティ名を返します。
         * 
         * @return rejiListのプロパティ名
         */
        public _RejiNames rejiList() {
            return new _RejiNames(this, "rejiList");
        }
    }
}