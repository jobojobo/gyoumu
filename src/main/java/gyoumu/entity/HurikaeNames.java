package gyoumu.entity;

import gyoumu.entity.DvdNames._DvdNames;
import gyoumu.entity.TenpoNames._TenpoNames;
import java.util.Date;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Hurikae}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class HurikaeNames {

    /**
     * tenponoのプロパティ名を返します。
     * 
     * @return tenponoのプロパティ名
     */
    public static PropertyName<String> tenpono() {
        return new PropertyName<String>("tenpono");
    }

    /**
     * sinseidayのプロパティ名を返します。
     * 
     * @return sinseidayのプロパティ名
     */
    public static PropertyName<Date> sinseiday() {
        return new PropertyName<Date>("sinseiday");
    }

    /**
     * dvdnoのプロパティ名を返します。
     * 
     * @return dvdnoのプロパティ名
     */
    public static PropertyName<String> dvdno() {
        return new PropertyName<String>("dvdno");
    }

    /**
     * shoninflgのプロパティ名を返します。
     * 
     * @return shoninflgのプロパティ名
     */
    public static PropertyName<String> shoninflg() {
        return new PropertyName<String>("shoninflg");
    }

    /**
     * kashidasidayのプロパティ名を返します。
     * 
     * @return kashidasidayのプロパティ名
     */
    public static PropertyName<Date> kashidasiday() {
        return new PropertyName<Date>("kashidasiday");
    }

    /**
     * henkyakuyoteidayのプロパティ名を返します。
     * 
     * @return henkyakuyoteidayのプロパティ名
     */
    public static PropertyName<Date> henkyakuyoteiday() {
        return new PropertyName<Date>("henkyakuyoteiday");
    }

    /**
     * henkyakudayのプロパティ名を返します。
     * 
     * @return henkyakudayのプロパティ名
     */
    public static PropertyName<Date> henkyakuday() {
        return new PropertyName<Date>("henkyakuday");
    }

    /**
     * dvdのプロパティ名を返します。
     * 
     * @return dvdのプロパティ名
     */
    public static _DvdNames dvd() {
        return new _DvdNames("dvd");
    }

    /**
     * tenpoのプロパティ名を返します。
     * 
     * @return tenpoのプロパティ名
     */
    public static _TenpoNames tenpo() {
        return new _TenpoNames("tenpo");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _HurikaeNames extends PropertyName<Hurikae> {

        /**
         * インスタンスを構築します。
         */
        public _HurikaeNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _HurikaeNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _HurikaeNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * tenponoのプロパティ名を返します。
         *
         * @return tenponoのプロパティ名
         */
        public PropertyName<String> tenpono() {
            return new PropertyName<String>(this, "tenpono");
        }

        /**
         * sinseidayのプロパティ名を返します。
         *
         * @return sinseidayのプロパティ名
         */
        public PropertyName<Date> sinseiday() {
            return new PropertyName<Date>(this, "sinseiday");
        }

        /**
         * dvdnoのプロパティ名を返します。
         *
         * @return dvdnoのプロパティ名
         */
        public PropertyName<String> dvdno() {
            return new PropertyName<String>(this, "dvdno");
        }

        /**
         * shoninflgのプロパティ名を返します。
         *
         * @return shoninflgのプロパティ名
         */
        public PropertyName<String> shoninflg() {
            return new PropertyName<String>(this, "shoninflg");
        }

        /**
         * kashidasidayのプロパティ名を返します。
         *
         * @return kashidasidayのプロパティ名
         */
        public PropertyName<Date> kashidasiday() {
            return new PropertyName<Date>(this, "kashidasiday");
        }

        /**
         * henkyakuyoteidayのプロパティ名を返します。
         *
         * @return henkyakuyoteidayのプロパティ名
         */
        public PropertyName<Date> henkyakuyoteiday() {
            return new PropertyName<Date>(this, "henkyakuyoteiday");
        }

        /**
         * henkyakudayのプロパティ名を返します。
         *
         * @return henkyakudayのプロパティ名
         */
        public PropertyName<Date> henkyakuday() {
            return new PropertyName<Date>(this, "henkyakuday");
        }

        /**
         * dvdのプロパティ名を返します。
         * 
         * @return dvdのプロパティ名
         */
        public _DvdNames dvd() {
            return new _DvdNames(this, "dvd");
        }

        /**
         * tenpoのプロパティ名を返します。
         * 
         * @return tenpoのプロパティ名
         */
        public _TenpoNames tenpo() {
            return new _TenpoNames(this, "tenpo");
        }
    }
}