package gyoumu.entity;

import gyoumu.entity.KaiinNames._KaiinNames;
import gyoumu.entity.KaikeiNames._KaikeiNames;
import gyoumu.entity.KasidasimeisaiNames._KasidasimeisaiNames;
import gyoumu.entity.RejiNames._RejiNames;
import gyoumu.entity.ShiharaikubunNames._ShiharaikubunNames;
import gyoumu.entity.TeninNames._TeninNames;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Kaikei}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class KaikeiNames {

    /**
     * kaikeinoのプロパティ名を返します。
     * 
     * @return kaikeinoのプロパティ名
     */
    public static PropertyName<String> kaikeino() {
        return new PropertyName<String>("kaikeino");
    }

    /**
     * kaikeikubunのプロパティ名を返します。
     * 
     * @return kaikeikubunのプロパティ名
     */
    public static PropertyName<String> kaikeikubun() {
        return new PropertyName<String>("kaikeikubun");
    }

    /**
     * kaikeinengappiのプロパティ名を返します。
     * 
     * @return kaikeinengappiのプロパティ名
     */
    public static PropertyName<Date> kaikeinengappi() {
        return new PropertyName<Date>("kaikeinengappi");
    }

    /**
     * kingakuのプロパティ名を返します。
     * 
     * @return kingakuのプロパティ名
     */
    public static PropertyName<BigInteger> kingaku() {
        return new PropertyName<BigInteger>("kingaku");
    }

    /**
     * rejinoのプロパティ名を返します。
     * 
     * @return rejinoのプロパティ名
     */
    public static PropertyName<String> rejino() {
        return new PropertyName<String>("rejino");
    }

    /**
     * teninnoのプロパティ名を返します。
     * 
     * @return teninnoのプロパティ名
     */
    public static PropertyName<String> teninno() {
        return new PropertyName<String>("teninno");
    }

    /**
     * kaiinnoのプロパティ名を返します。
     * 
     * @return kaiinnoのプロパティ名
     */
    public static PropertyName<String> kaiinno() {
        return new PropertyName<String>("kaiinno");
    }

    /**
     * henkinnoのプロパティ名を返します。
     * 
     * @return henkinnoのプロパティ名
     */
    public static PropertyName<String> henkinno() {
        return new PropertyName<String>("henkinno");
    }

    /**
     * kaiinのプロパティ名を返します。
     * 
     * @return kaiinのプロパティ名
     */
    public static _KaiinNames kaiin() {
        return new _KaiinNames("kaiin");
    }

    /**
     * kaikeiのプロパティ名を返します。
     * 
     * @return kaikeiのプロパティ名
     */
    public static _KaikeiNames kaikei() {
        return new _KaikeiNames("kaikei");
    }

    /**
     * kaikeiListのプロパティ名を返します。
     * 
     * @return kaikeiListのプロパティ名
     */
    public static _KaikeiNames kaikeiList() {
        return new _KaikeiNames("kaikeiList");
    }

    /**
     * rejiのプロパティ名を返します。
     * 
     * @return rejiのプロパティ名
     */
    public static _RejiNames reji() {
        return new _RejiNames("reji");
    }

    /**
     * shiharaikubunのプロパティ名を返します。
     * 
     * @return shiharaikubunのプロパティ名
     */
    public static _ShiharaikubunNames shiharaikubun() {
        return new _ShiharaikubunNames("shiharaikubun");
    }

    /**
     * teninのプロパティ名を返します。
     * 
     * @return teninのプロパティ名
     */
    public static _TeninNames tenin() {
        return new _TeninNames("tenin");
    }

    /**
     * kasidasimeisaiListのプロパティ名を返します。
     * 
     * @return kasidasimeisaiListのプロパティ名
     */
    public static _KasidasimeisaiNames kasidasimeisaiList() {
        return new _KasidasimeisaiNames("kasidasimeisaiList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _KaikeiNames extends PropertyName<Kaikei> {

        /**
         * インスタンスを構築します。
         */
        public _KaikeiNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _KaikeiNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _KaikeiNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * kaikeinoのプロパティ名を返します。
         *
         * @return kaikeinoのプロパティ名
         */
        public PropertyName<String> kaikeino() {
            return new PropertyName<String>(this, "kaikeino");
        }

        /**
         * kaikeikubunのプロパティ名を返します。
         *
         * @return kaikeikubunのプロパティ名
         */
        public PropertyName<String> kaikeikubun() {
            return new PropertyName<String>(this, "kaikeikubun");
        }

        /**
         * kaikeinengappiのプロパティ名を返します。
         *
         * @return kaikeinengappiのプロパティ名
         */
        public PropertyName<Date> kaikeinengappi() {
            return new PropertyName<Date>(this, "kaikeinengappi");
        }

        /**
         * kingakuのプロパティ名を返します。
         *
         * @return kingakuのプロパティ名
         */
        public PropertyName<BigInteger> kingaku() {
            return new PropertyName<BigInteger>(this, "kingaku");
        }

        /**
         * rejinoのプロパティ名を返します。
         *
         * @return rejinoのプロパティ名
         */
        public PropertyName<String> rejino() {
            return new PropertyName<String>(this, "rejino");
        }

        /**
         * teninnoのプロパティ名を返します。
         *
         * @return teninnoのプロパティ名
         */
        public PropertyName<String> teninno() {
            return new PropertyName<String>(this, "teninno");
        }

        /**
         * kaiinnoのプロパティ名を返します。
         *
         * @return kaiinnoのプロパティ名
         */
        public PropertyName<String> kaiinno() {
            return new PropertyName<String>(this, "kaiinno");
        }

        /**
         * henkinnoのプロパティ名を返します。
         *
         * @return henkinnoのプロパティ名
         */
        public PropertyName<String> henkinno() {
            return new PropertyName<String>(this, "henkinno");
        }

        /**
         * kaiinのプロパティ名を返します。
         * 
         * @return kaiinのプロパティ名
         */
        public _KaiinNames kaiin() {
            return new _KaiinNames(this, "kaiin");
        }

        /**
         * kaikeiのプロパティ名を返します。
         * 
         * @return kaikeiのプロパティ名
         */
        public _KaikeiNames kaikei() {
            return new _KaikeiNames(this, "kaikei");
        }

        /**
         * kaikeiListのプロパティ名を返します。
         * 
         * @return kaikeiListのプロパティ名
         */
        public _KaikeiNames kaikeiList() {
            return new _KaikeiNames(this, "kaikeiList");
        }

        /**
         * rejiのプロパティ名を返します。
         * 
         * @return rejiのプロパティ名
         */
        public _RejiNames reji() {
            return new _RejiNames(this, "reji");
        }

        /**
         * shiharaikubunのプロパティ名を返します。
         * 
         * @return shiharaikubunのプロパティ名
         */
        public _ShiharaikubunNames shiharaikubun() {
            return new _ShiharaikubunNames(this, "shiharaikubun");
        }

        /**
         * teninのプロパティ名を返します。
         * 
         * @return teninのプロパティ名
         */
        public _TeninNames tenin() {
            return new _TeninNames(this, "tenin");
        }

        /**
         * kasidasimeisaiListのプロパティ名を返します。
         * 
         * @return kasidasimeisaiListのプロパティ名
         */
        public _KasidasimeisaiNames kasidasimeisaiList() {
            return new _KasidasimeisaiNames(this, "kasidasimeisaiList");
        }
    }
}