package gyoumu.entity;

import gyoumu.entity.KaikeiNames._KaikeiNames;
import gyoumu.entity.RejisimeNames._RejisimeNames;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Tenin}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class TeninNames {

    /**
     * teninnoのプロパティ名を返します。
     * 
     * @return teninnoのプロパティ名
     */
    public static PropertyName<String> teninno() {
        return new PropertyName<String>("teninno");
    }

    /**
     * teninnameのプロパティ名を返します。
     * 
     * @return teninnameのプロパティ名
     */
    public static PropertyName<String> teninname() {
        return new PropertyName<String>("teninname");
    }

    /**
     * yakushokuのプロパティ名を返します。
     * 
     * @return yakushokuのプロパティ名
     */
    public static PropertyName<String> yakushoku() {
        return new PropertyName<String>("yakushoku");
    }

    /**
     * taisyaflgのプロパティ名を返します。
     * 
     * @return taisyaflgのプロパティ名
     */
    public static PropertyName<String> taisyaflg() {
        return new PropertyName<String>("taisyaflg");
    }

    /**
     * kaikeiListのプロパティ名を返します。
     * 
     * @return kaikeiListのプロパティ名
     */
    public static _KaikeiNames kaikeiList() {
        return new _KaikeiNames("kaikeiList");
    }

    /**
     * rejisimeListのプロパティ名を返します。
     * 
     * @return rejisimeListのプロパティ名
     */
    public static _RejisimeNames rejisimeList() {
        return new _RejisimeNames("rejisimeList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _TeninNames extends PropertyName<Tenin> {

        /**
         * インスタンスを構築します。
         */
        public _TeninNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _TeninNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _TeninNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * teninnoのプロパティ名を返します。
         *
         * @return teninnoのプロパティ名
         */
        public PropertyName<String> teninno() {
            return new PropertyName<String>(this, "teninno");
        }

        /**
         * teninnameのプロパティ名を返します。
         *
         * @return teninnameのプロパティ名
         */
        public PropertyName<String> teninname() {
            return new PropertyName<String>(this, "teninname");
        }

        /**
         * yakushokuのプロパティ名を返します。
         *
         * @return yakushokuのプロパティ名
         */
        public PropertyName<String> yakushoku() {
            return new PropertyName<String>(this, "yakushoku");
        }

        /**
         * taisyaflgのプロパティ名を返します。
         *
         * @return taisyaflgのプロパティ名
         */
        public PropertyName<String> taisyaflg() {
            return new PropertyName<String>(this, "taisyaflg");
        }

        /**
         * kaikeiListのプロパティ名を返します。
         * 
         * @return kaikeiListのプロパティ名
         */
        public _KaikeiNames kaikeiList() {
            return new _KaikeiNames(this, "kaikeiList");
        }

        /**
         * rejisimeListのプロパティ名を返します。
         * 
         * @return rejisimeListのプロパティ名
         */
        public _RejisimeNames rejisimeList() {
            return new _RejisimeNames(this, "rejisimeList");
        }
    }
}