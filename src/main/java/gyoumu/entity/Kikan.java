package gyoumu.entity;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Kikanエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Kikan implements Serializable {

    private static final long serialVersionUID = 1L;

    /** hakusuプロパティ */
    @Id
    @Column(length = 3, nullable = false, unique = true)
    public String hakusu;

    /** hakusunameプロパティ */
    @Column(length = 30, nullable = false, unique = false)
    public String hakusuname;

    /** kasidasimeisaiList関連プロパティ */
    @OneToMany(mappedBy = "kikan")
    public List<Kasidasimeisai> kasidasimeisaiList;

    /** ryokinList関連プロパティ */
    @OneToMany(mappedBy = "kikan")
    public List<Ryokin> ryokinList;
}