package gyoumu.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Rejisimeエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Rejisime implements Serializable {

    private static final long serialVersionUID = 1L;

    /** rejinoプロパティ */
    @Id
    @Column(length = 6, nullable = false, unique = false)
    public String rejino;

    /** rejisimedayプロパティ */
    @Id
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, unique = false)
    public Date rejisimeday;

    /** kingakuプロパティ */
    @Column(precision = 38, nullable = false, unique = false)
    public BigInteger kingaku;

    /** teninnoプロパティ */
    @Column(length = 6, nullable = false, unique = false)
    public String teninno;

    /** reji関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "REJINO", referencedColumnName = "REJINO")
    public Reji reji;

    /** tenin関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "TENINNO", referencedColumnName = "TENINNO")
    public Tenin tenin;
}