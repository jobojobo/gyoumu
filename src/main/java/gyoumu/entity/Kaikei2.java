package gyoumu.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Kaikeiエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Kaikei2 implements Serializable {

    private static final long serialVersionUID = 1L;

    /** kaikeinoプロパティ */
    @Id
    @Column(length = 10, nullable = false, unique = true)
    public String kaikeino;

    /** kaikeikubunプロパティ */
    @Column(length = 2, nullable = false, unique = false)
    public String kaikeikubun;

    /** kaikeinengappiプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, unique = false)
    public Date kaikeinengappi;

    /** kingakuプロパティ */
    @Column(precision = 38, nullable = true, unique = false)
    public BigInteger kingaku;

    /** kingakuプロパティ */
    @Column(precision = 38, nullable = true, unique = false)
    public BigInteger kasidasimeisaino;
    
    /** kingakuプロパティ */
    @Column(precision = 38, nullable = true, unique = false)
    public BigInteger dvdno;
    
    /** kaikeinengappiプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, unique = false)
    public Date henkyakuday;
    
    /** kaikeinengappiプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, unique = false)
    public Date henkyakuyoteiday;
    
    
    /** rejinoプロパティ */
    @Column(length = 6, nullable = false, unique = false)
    public String hakusu;
    
    /** rejinoプロパティ */
    @Column(length = 6, nullable = false, unique = false)
    public String tanka;
    
    /** rejinoプロパティ */
    @Column(length = 6, nullable = false, unique = false)
    public String waribikino;
    
    /** rejinoプロパティ */
    @Column(length = 6, nullable = false, unique = false)
    public String tenpono;
    
    /** rejinoプロパティ */
    @Column(length = 6, nullable = false, unique = false)
    public String rejino;

    /** teninnoプロパティ */
    @Column(length = 6, nullable = false, unique = false)
    public String teninno;

    /** kaiinnoプロパティ */
    @Column(length = 6, nullable = false, unique = false)
    public String kaiinno;

    /** henkinnoプロパティ */
    @Column(length = 10, nullable = true, unique = false)
    public String henkinno;

    /** kaiin関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "KAIINNO", referencedColumnName = "KAIINNO")
    public Kaiin kaiin;

    /** kaikei関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "HENKINNO", referencedColumnName = "KAIKEINO")
    public Kaikei kaikei;

    /** kaikeiList関連プロパティ */
    @OneToMany(mappedBy = "kaikei")
    public List<Kaikei> kaikeiList;

    /** reji関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "REJINO", referencedColumnName = "REJINO")
    public Reji reji;

    /** shiharaikubun関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "KAIKEIKUBUN", referencedColumnName = "KAIKEIKUBUN")
    public Shiharaikubun shiharaikubun;

    /** tenin関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "TENINNO", referencedColumnName = "TENINNO")
    public Tenin tenin;

    /** kasidasimeisaiList関連プロパティ */
    @OneToMany(mappedBy = "kaikei")
    public List<Kasidasimeisai> kasidasimeisaiList;
    
    /** kasidasimeisai関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "KASIDASIMEISAINO", referencedColumnName = "KASIDASIMEISAINO")
    public Kasidasimeisai kasidasimeisai;

}