package gyoumu.entity;

import gyoumu.entity.SakuhinNames._SakuhinNames;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Jyanru}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class JyanruNames {

    /**
     * jyanrunoのプロパティ名を返します。
     * 
     * @return jyanrunoのプロパティ名
     */
    public static PropertyName<String> jyanruno() {
        return new PropertyName<String>("jyanruno");
    }

    /**
     * jyanrunameのプロパティ名を返します。
     * 
     * @return jyanrunameのプロパティ名
     */
    public static PropertyName<String> jyanruname() {
        return new PropertyName<String>("jyanruname");
    }

    /**
     * sakuhinListのプロパティ名を返します。
     * 
     * @return sakuhinListのプロパティ名
     */
    public static _SakuhinNames sakuhinList() {
        return new _SakuhinNames("sakuhinList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _JyanruNames extends PropertyName<Jyanru> {

        /**
         * インスタンスを構築します。
         */
        public _JyanruNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _JyanruNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _JyanruNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * jyanrunoのプロパティ名を返します。
         *
         * @return jyanrunoのプロパティ名
         */
        public PropertyName<String> jyanruno() {
            return new PropertyName<String>(this, "jyanruno");
        }

        /**
         * jyanrunameのプロパティ名を返します。
         *
         * @return jyanrunameのプロパティ名
         */
        public PropertyName<String> jyanruname() {
            return new PropertyName<String>(this, "jyanruname");
        }

        /**
         * sakuhinListのプロパティ名を返します。
         * 
         * @return sakuhinListのプロパティ名
         */
        public _SakuhinNames sakuhinList() {
            return new _SakuhinNames(this, "sakuhinList");
        }
    }
}