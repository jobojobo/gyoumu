package gyoumu.entity;

import gyoumu.entity.KaikeiNames._KaikeiNames;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Shiharaikubun}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class ShiharaikubunNames {

    /**
     * kaikeikubunのプロパティ名を返します。
     * 
     * @return kaikeikubunのプロパティ名
     */
    public static PropertyName<String> kaikeikubun() {
        return new PropertyName<String>("kaikeikubun");
    }

    /**
     * kaikeikubunnameのプロパティ名を返します。
     * 
     * @return kaikeikubunnameのプロパティ名
     */
    public static PropertyName<String> kaikeikubunname() {
        return new PropertyName<String>("kaikeikubunname");
    }

    /**
     * kaikeiListのプロパティ名を返します。
     * 
     * @return kaikeiListのプロパティ名
     */
    public static _KaikeiNames kaikeiList() {
        return new _KaikeiNames("kaikeiList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _ShiharaikubunNames extends PropertyName<Shiharaikubun> {

        /**
         * インスタンスを構築します。
         */
        public _ShiharaikubunNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _ShiharaikubunNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _ShiharaikubunNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * kaikeikubunのプロパティ名を返します。
         *
         * @return kaikeikubunのプロパティ名
         */
        public PropertyName<String> kaikeikubun() {
            return new PropertyName<String>(this, "kaikeikubun");
        }

        /**
         * kaikeikubunnameのプロパティ名を返します。
         *
         * @return kaikeikubunnameのプロパティ名
         */
        public PropertyName<String> kaikeikubunname() {
            return new PropertyName<String>(this, "kaikeikubunname");
        }

        /**
         * kaikeiListのプロパティ名を返します。
         * 
         * @return kaikeiListのプロパティ名
         */
        public _KaikeiNames kaikeiList() {
            return new _KaikeiNames(this, "kaikeiList");
        }
    }
}