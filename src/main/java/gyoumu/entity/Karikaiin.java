package gyoumu.entity;

import java.io.Serializable;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Karikaiinエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Karikaiin implements Serializable {

    private static final long serialVersionUID = 1L;

    /** karikaiinnoプロパティ */
    @Id
    @Column(length = 6, nullable = false, unique = true)
    public String karikaiinno;

    /** kaiinnameプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String kaiinname;

    /** kaiinhuriganaプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String kaiinhurigana;

    /** genderプロパティ */
    @Column(length = 2, nullable = false, unique = false)
    public String gender;

    /** addressプロパティ */
    @Column(length = 160, nullable = false, unique = false)
    public String address;

    /** telプロパティ */
    @Column(length = 13, nullable = false, unique = false)
    public String tel;

    /** mailプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String mail;

    /** birthdayプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, unique = false)
    public Date birthday;

    /** shokugyoプロパティ */
    @Column(length = 16, nullable = false, unique = false)
    public String shokugyo;

    /** hakkoudayプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, unique = false)
    public Date hakkouday;
}