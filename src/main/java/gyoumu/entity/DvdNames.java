package gyoumu.entity;

import gyoumu.entity.HurikaeNames._HurikaeNames;
import gyoumu.entity.KasidasimeisaiNames._KasidasimeisaiNames;
import gyoumu.entity.SakuhinNames._SakuhinNames;
import gyoumu.entity.TenpoNames._TenpoNames;
import gyoumu.entity.ToriokiNames._ToriokiNames;
import java.util.Date;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Dvd}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class DvdNames {

    /**
     * dvdnoのプロパティ名を返します。
     * 
     * @return dvdnoのプロパティ名
     */
    public static PropertyName<String> dvdno() {
        return new PropertyName<String>("dvdno");
    }

    /**
     * sakuhinnoのプロパティ名を返します。
     * 
     * @return sakuhinnoのプロパティ名
     */
    public static PropertyName<String> sakuhinno() {
        return new PropertyName<String>("sakuhinno");
    }

    /**
     * kasidasistatusのプロパティ名を返します。
     * 
     * @return kasidasistatusのプロパティ名
     */
    public static PropertyName<String> kasidasistatus() {
        return new PropertyName<String>("kasidasistatus");
    }

    /**
     * nyuukodayのプロパティ名を返します。
     * 
     * @return nyuukodayのプロパティ名
     */
    public static PropertyName<Date> nyuukoday() {
        return new PropertyName<Date>("nyuukoday");
    }

    /**
     * baitaiのプロパティ名を返します。
     * 
     * @return baitaiのプロパティ名
     */
    public static PropertyName<String> baitai() {
        return new PropertyName<String>("baitai");
    }

    /**
     * tenponoのプロパティ名を返します。
     * 
     * @return tenponoのプロパティ名
     */
    public static PropertyName<String> tenpono() {
        return new PropertyName<String>("tenpono");
    }

    /**
     * sakuhinのプロパティ名を返します。
     * 
     * @return sakuhinのプロパティ名
     */
    public static _SakuhinNames sakuhin() {
        return new _SakuhinNames("sakuhin");
    }

    /**
     * tenpoのプロパティ名を返します。
     * 
     * @return tenpoのプロパティ名
     */
    public static _TenpoNames tenpo() {
        return new _TenpoNames("tenpo");
    }

    /**
     * hurikaeListのプロパティ名を返します。
     * 
     * @return hurikaeListのプロパティ名
     */
    public static _HurikaeNames hurikaeList() {
        return new _HurikaeNames("hurikaeList");
    }

    /**
     * kasidasimeisaiListのプロパティ名を返します。
     * 
     * @return kasidasimeisaiListのプロパティ名
     */
    public static _KasidasimeisaiNames kasidasimeisaiList() {
        return new _KasidasimeisaiNames("kasidasimeisaiList");
    }

    /**
     * toriokiListのプロパティ名を返します。
     * 
     * @return toriokiListのプロパティ名
     */
    public static _ToriokiNames toriokiList() {
        return new _ToriokiNames("toriokiList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _DvdNames extends PropertyName<Dvd> {

        /**
         * インスタンスを構築します。
         */
        public _DvdNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _DvdNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _DvdNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * dvdnoのプロパティ名を返します。
         *
         * @return dvdnoのプロパティ名
         */
        public PropertyName<String> dvdno() {
            return new PropertyName<String>(this, "dvdno");
        }

        /**
         * sakuhinnoのプロパティ名を返します。
         *
         * @return sakuhinnoのプロパティ名
         */
        public PropertyName<String> sakuhinno() {
            return new PropertyName<String>(this, "sakuhinno");
        }

        /**
         * kasidasistatusのプロパティ名を返します。
         *
         * @return kasidasistatusのプロパティ名
         */
        public PropertyName<String> kasidasistatus() {
            return new PropertyName<String>(this, "kasidasistatus");
        }

        /**
         * nyuukodayのプロパティ名を返します。
         *
         * @return nyuukodayのプロパティ名
         */
        public PropertyName<Date> nyuukoday() {
            return new PropertyName<Date>(this, "nyuukoday");
        }

        /**
         * baitaiのプロパティ名を返します。
         *
         * @return baitaiのプロパティ名
         */
        public PropertyName<String> baitai() {
            return new PropertyName<String>(this, "baitai");
        }

        /**
         * tenponoのプロパティ名を返します。
         *
         * @return tenponoのプロパティ名
         */
        public PropertyName<String> tenpono() {
            return new PropertyName<String>(this, "tenpono");
        }

        /**
         * sakuhinのプロパティ名を返します。
         * 
         * @return sakuhinのプロパティ名
         */
        public _SakuhinNames sakuhin() {
            return new _SakuhinNames(this, "sakuhin");
        }

        /**
         * tenpoのプロパティ名を返します。
         * 
         * @return tenpoのプロパティ名
         */
        public _TenpoNames tenpo() {
            return new _TenpoNames(this, "tenpo");
        }

        /**
         * hurikaeListのプロパティ名を返します。
         * 
         * @return hurikaeListのプロパティ名
         */
        public _HurikaeNames hurikaeList() {
            return new _HurikaeNames(this, "hurikaeList");
        }

        /**
         * kasidasimeisaiListのプロパティ名を返します。
         * 
         * @return kasidasimeisaiListのプロパティ名
         */
        public _KasidasimeisaiNames kasidasimeisaiList() {
            return new _KasidasimeisaiNames(this, "kasidasimeisaiList");
        }

        /**
         * toriokiListのプロパティ名を返します。
         * 
         * @return toriokiListのプロパティ名
         */
        public _ToriokiNames toriokiList() {
            return new _ToriokiNames(this, "toriokiList");
        }
    }
}