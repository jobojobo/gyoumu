package gyoumu.entity;

import java.util.Date;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Karikaiin}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class KarikaiinNames {

    /**
     * karikaiinnoのプロパティ名を返します。
     * 
     * @return karikaiinnoのプロパティ名
     */
    public static PropertyName<String> karikaiinno() {
        return new PropertyName<String>("karikaiinno");
    }

    /**
     * kaiinnameのプロパティ名を返します。
     * 
     * @return kaiinnameのプロパティ名
     */
    public static PropertyName<String> kaiinname() {
        return new PropertyName<String>("kaiinname");
    }

    /**
     * kaiinhuriganaのプロパティ名を返します。
     * 
     * @return kaiinhuriganaのプロパティ名
     */
    public static PropertyName<String> kaiinhurigana() {
        return new PropertyName<String>("kaiinhurigana");
    }

    /**
     * genderのプロパティ名を返します。
     * 
     * @return genderのプロパティ名
     */
    public static PropertyName<String> gender() {
        return new PropertyName<String>("gender");
    }

    /**
     * addressのプロパティ名を返します。
     * 
     * @return addressのプロパティ名
     */
    public static PropertyName<String> address() {
        return new PropertyName<String>("address");
    }

    /**
     * telのプロパティ名を返します。
     * 
     * @return telのプロパティ名
     */
    public static PropertyName<String> tel() {
        return new PropertyName<String>("tel");
    }

    /**
     * mailのプロパティ名を返します。
     * 
     * @return mailのプロパティ名
     */
    public static PropertyName<String> mail() {
        return new PropertyName<String>("mail");
    }

    /**
     * birthdayのプロパティ名を返します。
     * 
     * @return birthdayのプロパティ名
     */
    public static PropertyName<Date> birthday() {
        return new PropertyName<Date>("birthday");
    }

    /**
     * shokugyoのプロパティ名を返します。
     * 
     * @return shokugyoのプロパティ名
     */
    public static PropertyName<String> shokugyo() {
        return new PropertyName<String>("shokugyo");
    }

    /**
     * hakkoudayのプロパティ名を返します。
     * 
     * @return hakkoudayのプロパティ名
     */
    public static PropertyName<Date> hakkouday() {
        return new PropertyName<Date>("hakkouday");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _KarikaiinNames extends PropertyName<Karikaiin> {

        /**
         * インスタンスを構築します。
         */
        public _KarikaiinNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _KarikaiinNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _KarikaiinNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * karikaiinnoのプロパティ名を返します。
         *
         * @return karikaiinnoのプロパティ名
         */
        public PropertyName<String> karikaiinno() {
            return new PropertyName<String>(this, "karikaiinno");
        }

        /**
         * kaiinnameのプロパティ名を返します。
         *
         * @return kaiinnameのプロパティ名
         */
        public PropertyName<String> kaiinname() {
            return new PropertyName<String>(this, "kaiinname");
        }

        /**
         * kaiinhuriganaのプロパティ名を返します。
         *
         * @return kaiinhuriganaのプロパティ名
         */
        public PropertyName<String> kaiinhurigana() {
            return new PropertyName<String>(this, "kaiinhurigana");
        }

        /**
         * genderのプロパティ名を返します。
         *
         * @return genderのプロパティ名
         */
        public PropertyName<String> gender() {
            return new PropertyName<String>(this, "gender");
        }

        /**
         * addressのプロパティ名を返します。
         *
         * @return addressのプロパティ名
         */
        public PropertyName<String> address() {
            return new PropertyName<String>(this, "address");
        }

        /**
         * telのプロパティ名を返します。
         *
         * @return telのプロパティ名
         */
        public PropertyName<String> tel() {
            return new PropertyName<String>(this, "tel");
        }

        /**
         * mailのプロパティ名を返します。
         *
         * @return mailのプロパティ名
         */
        public PropertyName<String> mail() {
            return new PropertyName<String>(this, "mail");
        }

        /**
         * birthdayのプロパティ名を返します。
         *
         * @return birthdayのプロパティ名
         */
        public PropertyName<Date> birthday() {
            return new PropertyName<Date>(this, "birthday");
        }

        /**
         * shokugyoのプロパティ名を返します。
         *
         * @return shokugyoのプロパティ名
         */
        public PropertyName<String> shokugyo() {
            return new PropertyName<String>(this, "shokugyo");
        }

        /**
         * hakkoudayのプロパティ名を返します。
         *
         * @return hakkoudayのプロパティ名
         */
        public PropertyName<Date> hakkouday() {
            return new PropertyName<Date>(this, "hakkouday");
        }
    }
}