package gyoumu.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Kasidasimeisaiエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Kasidasimeisai implements Serializable {

    private static final long serialVersionUID = 1L;

    /** kaikeinoプロパティ */
    @Id
    @Column(length = 10, nullable = false, unique = false)
    public String kaikeino;

    /** kasidasimeisainoプロパティ */
    @Id
    @Column(length = 2, nullable = false, unique = false)
    public String kasidasimeisaino;

    /** dvdnoプロパティ */
    @Column(length = 13, nullable = false, unique = false)
    public String dvdno;

    /** henkyakudayプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true, unique = false)
    public Date henkyakuday;

    /** henkyakuyoteidayプロパティ */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, unique = false)
    public Date henkyakuyoteiday;

    /** hakusuプロパティ */
    @Column(length = 3, nullable = false, unique = false)
    public String hakusu;

    /** tankaプロパティ */
    @Column(precision = 38, nullable = false, unique = false)
    public BigInteger tanka;

    /** waribikinoプロパティ */
    @Column(length = 6, nullable = false, unique = false)
    public String waribikino;

    /** dvd関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "DVDNO", referencedColumnName = "DVDNO")
    public Dvd dvd;

    /** kaikei関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "KAIKEINO", referencedColumnName = "KAIKEINO")
    public Kaikei kaikei;

    /** kikan関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "HAKUSU", referencedColumnName = "HAKUSU")
    public Kikan kikan;

    /** waribiki関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "WARIBIKINO", referencedColumnName = "WARIBIKINO")
    public Waribiki waribiki;
    
    /** waribiki関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "KASIKEIKUBUN", referencedColumnName = "KAIKEIKUBUN")
    public Shiharaikubun kaikeikubun;
}