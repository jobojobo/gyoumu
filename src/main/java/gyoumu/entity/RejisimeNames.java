package gyoumu.entity;

import gyoumu.entity.RejiNames._RejiNames;
import gyoumu.entity.TeninNames._TeninNames;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Rejisime}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/08/31 15:52:49")
public class RejisimeNames {

    /**
     * rejinoのプロパティ名を返します。
     * 
     * @return rejinoのプロパティ名
     */
    public static PropertyName<String> rejino() {
        return new PropertyName<String>("rejino");
    }

    /**
     * rejisimedayのプロパティ名を返します。
     * 
     * @return rejisimedayのプロパティ名
     */
    public static PropertyName<Date> rejisimeday() {
        return new PropertyName<Date>("rejisimeday");
    }

    /**
     * kingakuのプロパティ名を返します。
     * 
     * @return kingakuのプロパティ名
     */
    public static PropertyName<BigInteger> kingaku() {
        return new PropertyName<BigInteger>("kingaku");
    }

    /**
     * teninnoのプロパティ名を返します。
     * 
     * @return teninnoのプロパティ名
     */
    public static PropertyName<String> teninno() {
        return new PropertyName<String>("teninno");
    }

    /**
     * rejiのプロパティ名を返します。
     * 
     * @return rejiのプロパティ名
     */
    public static _RejiNames reji() {
        return new _RejiNames("reji");
    }

    /**
     * teninのプロパティ名を返します。
     * 
     * @return teninのプロパティ名
     */
    public static _TeninNames tenin() {
        return new _TeninNames("tenin");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _RejisimeNames extends PropertyName<Rejisime> {

        /**
         * インスタンスを構築します。
         */
        public _RejisimeNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _RejisimeNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _RejisimeNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * rejinoのプロパティ名を返します。
         *
         * @return rejinoのプロパティ名
         */
        public PropertyName<String> rejino() {
            return new PropertyName<String>(this, "rejino");
        }

        /**
         * rejisimedayのプロパティ名を返します。
         *
         * @return rejisimedayのプロパティ名
         */
        public PropertyName<Date> rejisimeday() {
            return new PropertyName<Date>(this, "rejisimeday");
        }

        /**
         * kingakuのプロパティ名を返します。
         *
         * @return kingakuのプロパティ名
         */
        public PropertyName<BigInteger> kingaku() {
            return new PropertyName<BigInteger>(this, "kingaku");
        }

        /**
         * teninnoのプロパティ名を返します。
         *
         * @return teninnoのプロパティ名
         */
        public PropertyName<String> teninno() {
            return new PropertyName<String>(this, "teninno");
        }

        /**
         * rejiのプロパティ名を返します。
         * 
         * @return rejiのプロパティ名
         */
        public _RejiNames reji() {
            return new _RejiNames(this, "reji");
        }

        /**
         * teninのプロパティ名を返します。
         * 
         * @return teninのプロパティ名
         */
        public _TeninNames tenin() {
            return new _TeninNames(this, "tenin");
        }
    }
}