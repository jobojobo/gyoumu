package gyoumu.entity;

import java.io.Serializable;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Ryokinエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/08/31 15:52:46")
public class Ryokin implements Serializable {

    private static final long serialVersionUID = 1L;

    /** hakusuプロパティ */
    @Id
    @Column(length = 3, nullable = false, unique = false)
    public String hakusu;

    /** sinsakukubunプロパティ */
    @Id
    @Column(length = 1, nullable = false, unique = false)
    public String sinsakukubun;

    /** ryokinプロパティ */
    @Column(precision = 38, nullable = false, unique = false)
    public BigInteger ryokin;

    /** kikan関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "HAKUSU", referencedColumnName = "HAKUSU")
    public Kikan kikan;

    /** sakuhinkubun関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "SINSAKUKUBUN", referencedColumnName = "SINSAKUKUBUN")
    public Sakuhinkubun sakuhinkubun;
}