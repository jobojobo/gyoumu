package gyoumu.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.extension.unit.S2TestCase;

import static gyoumu.entity.KasidasimeisaiNames.*;

/**
 * {@link Kasidasimeisai}のテストクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityTestModelFactoryImpl"}, date = "2017/07/19 9:32:44")
public class KasidasimeisaiTest extends S2TestCase {

    private JdbcManager jdbcManager;

    /**
     * 事前処理をします。
     * 
     * @throws Exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        include("s2jdbc.dicon");
    }

    /**
     * 識別子による取得をテストします。
     * 
     * @throws Exception
     */
    public void testFindById() throws Exception {
        jdbcManager.from(Kasidasimeisai.class).id("aaa", "aaa").getSingleResult();
    }

    /**
     * dvdとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_dvd() throws Exception {
        jdbcManager.from(Kasidasimeisai.class).leftOuterJoin(dvd()).id("aaa", "aaa").getSingleResult();
    }

    /**
     * kaikeiとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_kaikei() throws Exception {
        jdbcManager.from(Kasidasimeisai.class).leftOuterJoin(kaikei()).id("aaa", "aaa").getSingleResult();
    }

    /**
     * kikanとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_kikan() throws Exception {
        jdbcManager.from(Kasidasimeisai.class).leftOuterJoin(kikan()).id("aaa", "aaa").getSingleResult();
    }

    /**
     * waribikiとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_waribiki() throws Exception {
        jdbcManager.from(Kasidasimeisai.class).leftOuterJoin(waribiki()).id("aaa", "aaa").getSingleResult();
    }
}