package gyoumu.entity;

import java.util.Date;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.extension.unit.S2TestCase;

import static gyoumu.entity.RejisimeNames.*;

/**
 * {@link Rejisime}のテストクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityTestModelFactoryImpl"}, date = "2017/07/19 9:32:44")
public class RejisimeTest extends S2TestCase {

    private JdbcManager jdbcManager;

    /**
     * 事前処理をします。
     * 
     * @throws Exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        include("s2jdbc.dicon");
    }

    /**
     * 識別子による取得をテストします。
     * 
     * @throws Exception
     */
    public void testFindById() throws Exception {
        jdbcManager.from(Rejisime.class).id("aaa", new Date()).getSingleResult();
    }

    /**
     * rejiとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_reji() throws Exception {
        jdbcManager.from(Rejisime.class).leftOuterJoin(reji()).id("aaa", new Date()).getSingleResult();
    }

    /**
     * teninとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_tenin() throws Exception {
        jdbcManager.from(Rejisime.class).leftOuterJoin(tenin()).id("aaa", new Date()).getSingleResult();
    }
}