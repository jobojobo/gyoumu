package gyoumu.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.extension.unit.S2TestCase;

import static gyoumu.entity.KaiinNames.*;

/**
 * {@link Kaiin}のテストクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityTestModelFactoryImpl"}, date = "2017/07/19 9:32:44")
public class KaiinTest extends S2TestCase {

    private JdbcManager jdbcManager;

    /**
     * 事前処理をします。
     * 
     * @throws Exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        include("s2jdbc.dicon");
    }

    /**
     * 識別子による取得をテストします。
     * 
     * @throws Exception
     */
    public void testFindById() throws Exception {
        jdbcManager.from(Kaiin.class).id("aaa").getSingleResult();
    }

    /**
     * mibunshoとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_mibunsho() throws Exception {
        jdbcManager.from(Kaiin.class).leftOuterJoin(mibunsho()).id("aaa").getSingleResult();
    }

    /**
     * kaikeiListとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_kaikeiList() throws Exception {
        jdbcManager.from(Kaiin.class).leftOuterJoin(kaikeiList()).id("aaa").getSingleResult();
    }
}