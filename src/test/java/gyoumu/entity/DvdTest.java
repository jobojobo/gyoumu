package gyoumu.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.extension.unit.S2TestCase;

import static gyoumu.entity.DvdNames.*;

/**
 * {@link Dvd}のテストクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityTestModelFactoryImpl"}, date = "2017/07/19 9:32:44")
public class DvdTest extends S2TestCase {

    private JdbcManager jdbcManager;

    /**
     * 事前処理をします。
     * 
     * @throws Exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        include("s2jdbc.dicon");
    }

    /**
     * 識別子による取得をテストします。
     * 
     * @throws Exception
     */
    public void testFindById() throws Exception {
        jdbcManager.from(Dvd.class).id("aaa").getSingleResult();
    }

    /**
     * sakuhinとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_sakuhin() throws Exception {
        jdbcManager.from(Dvd.class).leftOuterJoin(sakuhin()).id("aaa").getSingleResult();
    }

    /**
     * tenpoとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_tenpo() throws Exception {
        jdbcManager.from(Dvd.class).leftOuterJoin(tenpo()).id("aaa").getSingleResult();
    }

    /**
     * hurikaeListとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_hurikaeList() throws Exception {
        jdbcManager.from(Dvd.class).leftOuterJoin(hurikaeList()).id("aaa").getSingleResult();
    }

    /**
     * kasidasimeisaiListとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_kasidasimeisaiList() throws Exception {
        jdbcManager.from(Dvd.class).leftOuterJoin(kasidasimeisaiList()).id("aaa").getSingleResult();
    }

    /**
     * toriokiListとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_toriokiList() throws Exception {
        jdbcManager.from(Dvd.class).leftOuterJoin(toriokiList()).id("aaa").getSingleResult();
    }
}