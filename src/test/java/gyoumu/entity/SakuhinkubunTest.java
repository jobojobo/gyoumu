package gyoumu.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.extension.unit.S2TestCase;

import static gyoumu.entity.SakuhinkubunNames.*;

/**
 * {@link Sakuhinkubun}のテストクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityTestModelFactoryImpl"}, date = "2017/07/19 9:32:44")
public class SakuhinkubunTest extends S2TestCase {

    private JdbcManager jdbcManager;

    /**
     * 事前処理をします。
     * 
     * @throws Exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        include("s2jdbc.dicon");
    }

    /**
     * 識別子による取得をテストします。
     * 
     * @throws Exception
     */
    public void testFindById() throws Exception {
        jdbcManager.from(Sakuhinkubun.class).id("aaa").getSingleResult();
    }

    /**
     * ryokinListとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_ryokinList() throws Exception {
        jdbcManager.from(Sakuhinkubun.class).leftOuterJoin(ryokinList()).id("aaa").getSingleResult();
    }

    /**
     * sakuhinListとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_sakuhinList() throws Exception {
        jdbcManager.from(Sakuhinkubun.class).leftOuterJoin(sakuhinList()).id("aaa").getSingleResult();
    }
}