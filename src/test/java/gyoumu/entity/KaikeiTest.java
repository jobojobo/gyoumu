package gyoumu.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.extension.unit.S2TestCase;

import static gyoumu.entity.KaikeiNames.*;

/**
 * {@link Kaikei}のテストクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityTestModelFactoryImpl"}, date = "2017/07/19 9:32:44")
public class KaikeiTest extends S2TestCase {

    private JdbcManager jdbcManager;

    /**
     * 事前処理をします。
     * 
     * @throws Exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        include("s2jdbc.dicon");
    }

    /**
     * 識別子による取得をテストします。
     * 
     * @throws Exception
     */
    public void testFindById() throws Exception {
        jdbcManager.from(Kaikei.class).id("aaa").getSingleResult();
    }

    /**
     * kaiinとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_kaiin() throws Exception {
        jdbcManager.from(Kaikei.class).leftOuterJoin(kaiin()).id("aaa").getSingleResult();
    }

    /**
     * kaikeiとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_kaikei() throws Exception {
        jdbcManager.from(Kaikei.class).leftOuterJoin(kaikei()).id("aaa").getSingleResult();
    }

    /**
     * kaikeiListとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_kaikeiList() throws Exception {
        jdbcManager.from(Kaikei.class).leftOuterJoin(kaikeiList()).id("aaa").getSingleResult();
    }

    /**
     * kaikeikubunとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_kaikeikubun() throws Exception {
        jdbcManager.from(Kaikei.class).leftOuterJoin(kaikeikubun()).id("aaa").getSingleResult();
    }

    /**
     * rejiとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_reji() throws Exception {
        jdbcManager.from(Kaikei.class).leftOuterJoin(reji()).id("aaa").getSingleResult();
    }

    /**
     * teninとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_tenin() throws Exception {
        jdbcManager.from(Kaikei.class).leftOuterJoin(tenin()).id("aaa").getSingleResult();
    }

    /**
     * tenpoとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_tenpo() throws Exception {
        jdbcManager.from(Kaikei.class).leftOuterJoin(tenpo()).id("aaa").getSingleResult();
    }

    /**
     * kasidasimeisaiListとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_kasidasimeisaiList() throws Exception {
        jdbcManager.from(Kaikei.class).leftOuterJoin(kasidasimeisaiList()).id("aaa").getSingleResult();
    }
}