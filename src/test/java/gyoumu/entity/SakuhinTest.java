package gyoumu.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.extension.unit.S2TestCase;

import static gyoumu.entity.SakuhinNames.*;

/**
 * {@link Sakuhin}のテストクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityTestModelFactoryImpl"}, date = "2017/07/19 9:32:44")
public class SakuhinTest extends S2TestCase {

    private JdbcManager jdbcManager;

    /**
     * 事前処理をします。
     * 
     * @throws Exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        include("s2jdbc.dicon");
    }

    /**
     * 識別子による取得をテストします。
     * 
     * @throws Exception
     */
    public void testFindById() throws Exception {
        jdbcManager.from(Sakuhin.class).id("aaa").getSingleResult();
    }

    /**
     * dvdListとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_dvdList() throws Exception {
        jdbcManager.from(Sakuhin.class).leftOuterJoin(dvdList()).id("aaa").getSingleResult();
    }

    /**
     * jyanruとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_jyanru() throws Exception {
        jdbcManager.from(Sakuhin.class).leftOuterJoin(jyanru()).id("aaa").getSingleResult();
    }

    /**
     * sakuhinkubunとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_sakuhinkubun() throws Exception {
        jdbcManager.from(Sakuhin.class).leftOuterJoin(sakuhinkubun()).id("aaa").getSingleResult();
    }

    /**
     * toriokiListとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_toriokiList() throws Exception {
        jdbcManager.from(Sakuhin.class).leftOuterJoin(toriokiList()).id("aaa").getSingleResult();
    }
}